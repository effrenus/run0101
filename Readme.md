## Разворачивание проекта для локальной разработки

### Рекомендации

*nix система с версией ядра 3.10+ (`uname -r`) или macOS 10.8+, docker machine (+ virtualbox) под Windows

### Утилиты для работы

* git
* make
* docker (17+ community edition) [установка](https://docs.docker.com/engine/installation/)
* docker-compose (1.17+) [установка](https://docs.docker.com/compose/install/)
* (?) docker machine, virtualbox (с поддержкой vmx (intel): `cat /proc/cpuinfo | grep vmx`)

### Разворачивание

Фикстуры зашифрованы, поэтому для разворачивания дев окружения необходимо определить переменную окружения `DUMP_FILES_PASS` с паролем: `export DUMP_FILES_PASS=request_password` (запросить отдельно).

```bash
git clone https://effrenus@bitbucket.org/effrenus/run0101.git run0101 \
    && cd "$_"
# Достаточно вызвать один раз при самом первом запуске проекта.
make init

# Создание изображений (docker images) для последующего запуска контейнеров.
make build

# Создание контейнеров, запуск сервисов.
make run
# После успешного запуска доступен адрес http://localhost:8000/
# Страницы с `/2018/` в адресе идентичны текущим с http://run0101.com/2018/

# Остановка контейнеров.
# Cmd/Ctrl+C and
make stop 

# Чистка неактивных контейнеров и изображений.
make clean
```