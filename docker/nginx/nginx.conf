server {
  listen 80 default_server;
  listen [::]:80 default_server;

  resolver 127.0.0.11;

  root /project;
  index index.js index.php index.html;
  try_files $uri $uri/ @node;

  server_name _;

  server_tokens off;
  add_header X-Frame-Options "SAMEORIGIN";
  add_header X-Content-Type-Options nosniff;
  add_header X-XSS-Protection "1; mode=block";
  add_header Referrer-Policy "same-origin";

  gzip on;
  gzip_comp_level 6;
  gzip_min_length 512;
  gzip_proxied any;
  gzip_types application/javascript application/x-javascript application/json image/svg+xml text/css text/plain;

  location /2018/ {
    try_files $uri $uri/ @web;
  }

  location /static/ {
    alias /shared/node-static/;
    expires 10d;
  }

  location /static/-/ {
    alias /shared/php-static/;
    expires 10d;
  }

  location @web {
    root /var/www/html;
    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    fastcgi_pass php:9000;
    fastcgi_param SCRIPT_FILENAME $document_root/index.php;
    fastcgi_param SCRIPT_NAME $fastcgi_script_name;
    fastcgi_index index.php;
    include fastcgi_params;
  }

  location @node {
    set $upstream http://node:8000;

    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $http_host;
    proxy_redirect off;
    proxy_pass $upstream;
  }

  # deny access to . files, for security
  location ~ /\. {
    log_not_found off;
    deny all;
  }

  location ^~ /.well-known {
    allow all;
    auth_basic off;
  }
}
