#!/bin/sh
set -e

cd /var/www/html

npm install --silent --no-optional \
  && npm run clean

node ensure-mongo-running.js

npm run build:server
npm run build:client

if [ -f server/index.compiled.js ]; then
  npm run watch:server &
  npm run watch:client &
  exec ./node_modules/.bin/pm2-docker \
          -i 1 \
          --watch \
          --wait-ready \
          --listen-timeout 500000 \
          --no-daemon \
          start server/index.compiled.js
else
  echo "Can't find server/index.compiled.js file"
  exit 1
fi
