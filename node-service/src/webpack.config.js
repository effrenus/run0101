'use strict';

const prodConfig = require('./config/webpack.prod');
const devConfig = require('./config/webpack.dev');

module.exports = process.env.NODE_ENV !== 'production' ? devConfig : prodConfig;
