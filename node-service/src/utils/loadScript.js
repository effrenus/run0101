// @flow
export default function loadScript (url: string): Promise<*> {
  return new Promise((resolve, reject) => {
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.onload = resolve;
    script.onerror = reject;
    script.src = url;

    if (document.head) {
      document.head.appendChild(script);
    }
  });
}
