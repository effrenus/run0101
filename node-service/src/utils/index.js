// @flow
import type { LocationType, ActivitiesType } from '../client/reducers/activitiesData';

export function createGeoJson (locations: Array<LocationType>, activities: ActivitiesType): FeatureCollection {
  return {
    type: 'FeatureCollection',
    features: locations.map((location: LocationType, index): Feature => ({
      type: 'Feature',
      id: location.id,
      geometry: {
        type: 'Point', coordinates: location.coords
      },
      properties: {
        country: location.country,
        location: location.location,
        weather: location.weather,
        totalDistance: activities.reduce((res, activity) => (activity.locationId == location.id) ? res + activity.distance : res, 0)
      }
    }))
  };
}

/**
 * From https://github.com/web-push-libs/web-push
 */
export function urlBase64ToUint8Array (base64String: string): Uint8Array {
  const padding = '='.repeat((4 - (base64String.length % 4)) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; i += 1) {
    outputArray[i] = rawData.charCodeAt(i);
  }

  return outputArray;
}

export function numberWithSpace (number: number): string {
  return String(number).replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
}
