import fs from 'fs';
import path from 'path';
import { models } from 'mongoose';
import '../server/db';

function prepareData (item) {
  const processed = {};
  Object.keys(item).forEach((key) => { processed[key] = item[key].trim(); });
  ['sex', 'year', 'km', 'groupId', 'isValid', 'id'].forEach((key) => { processed[key] = parseInt(processed[key], 10); });
  ['name', 'surname', 'email'].forEach((key) => { processed[key] = processed[key].toLowerCase(); });
  processed.creationDate = new Date(processed.creationDate);
  processed.gender = processed.gender === 0 ? 2 : 1;

  return processed;
}

let fixtures = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../fixtures/dump.json')).toString());
fixtures = fixtures.map(prepareData);

const geocache = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../tools/data/geocache.json')).toString());

const users = [];

async function findUser (email) {
  // let r;
  // users.some((user) => {
  //   if (user.email === email) {
  //     r = user;
  //     return true;
  //   }
  //   return false;
  // });
  //
  // return r;
  return models.Participant.findOne({ email });
}

function findLocation (country, city) {
  let r = null;
  Object.keys(geocache).some((key) => {
    const { userLocality } = geocache[key];
    if (userLocality.toLowerCase() === `${country.trim()},${city.trim().split(',')[0]}`.toLowerCase()) {
      r = geocache[key].locality;
      return true;
    }
    return false;
  });
  return r;
}

function process () {
  fixtures
    .filter(item => item.isValid && item.email && item.groupId == 0 && item.year == 2017)
    .forEach(async (item) => {
      const { surname, name, email, country, city } = item;
      // if (!name) {
      //   return;
      // }
      let user = await findUser(email);

      if (!user) {
        return;

        // const p = new models.Participant({
        //   firstname: name,
        //   lastname: surname,
        //   email,
        //   gender: item.sex,
        //   created_at: item.creationDate
        // });
        //
        // await p.save(err => err && console.log(err));
        //
        // user = { email, inst: p };
        // users.push(user);
      }

      const act = new models.Activity({
        type: item.activity === 'run' ? 1 : 2,
        distance: item.km,
        comment: item.comment,
        created_at: item.creationDate,
        modified_at: item.creationDate,
        participantId: user
      });

      const l = findLocation(country, city);
      if (l) {
        models.Location.findByName(l.country, l.name).then((location) => {
          act.set('locationId', location);
          act.save(err => err && console.log(err));
        }).catch(err => act.save(err => err && console.log(err)));
      } else {
        act.save(err => err && console.log(err));
      }
    });
}

process();
