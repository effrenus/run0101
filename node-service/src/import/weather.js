/* eslint-disable  import/no-extraneous-dependencies, no-await-in-loop, no-console, no-constant-condition */
import got from 'got';
import { models } from 'mongoose';
import cheerio from 'cheerio';
import R from 'ramda';
import { asyncReduce, asyncCompose, asyncPartialRight, normalizeLocation } from './utils';
import '../server/db';
// Another variants: streams, rxjs.

const log = console.log;

const YEAR_FROM = 2014;
const YEAR_TO = 2017;
const SLEEP_TIME = 1; // Seconds.
const DOMEN = 'https://rp5.ru';

/**
 * Returns page object
 * @param  {string}  url     Page url.
 * @param  {object}  options
 * @return {string}         Page source.
 */
async function getPageBody (url, options) {
  options = options || {};
  const response = await got(url, options);
  return response.body;
}

/**
 * Convert html string to cheerio dom representation.
 * @param  {string} html Page html.
 * @return {object}      cheerio inst.
 */
function getPageDom (html) {
  return cheerio.load(html);
}

/**
 * Find location coords in page source code
 * @param  {object} $ cheerio object
 * @return {array[number, number]}   Coordinates.
 */
function findCoords ($) {
  const onclick = $('.iconmap+a').attr('onclick');
  const [lat, lng] = onclick.replace('show_map(', '').split(',').map(c => parseFloat(c));
  return [lat, lng];
}

const getCoordinates = asyncCompose(findCoords, getPageDom, getPageBody);

function linkToArchivePage ($) {
  return $('#ArchSynop a').attr('href').replace('http://', 'https://');
}

function getArchivePage (locationAlias, year) {
  const postOpts = {
    method: 'POST',
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    body: { ArchDate: `01.01.${year}`, pe: 1, lang: 'ru', time_zone_add: 3 }
  };

  return asyncCompose(
    asyncPartialRight(getPageBody, postOpts),
    linkToArchivePage,
    getPageDom,
    getPageBody
  )(`${DOMEN}/${encodeURI(locationAlias)}`);
}

function getTempOnPage ($) {
  return $('.cl_rd .t_0').eq(0).text();
}

const getLocationTemp = asyncCompose(getTempOnPage, getPageDom, getArchivePage);

function filterSuggestions (suggestJson, country) {
  return suggestJson
    .filter(suggest => suggest.name.split(',')[1].trim() === country);
}

function getFilteredSuggestions (url, country) {
  const filter = asyncCompose(
    R.partialRight(filterSuggestions, [country]),
    asyncPartialRight(getPageBody, { json: true })
  );
  return filter(url);
}

async function filterByCoords (suggestions, locationCoords) {
  const DELTA = 0.2;
  let result;
  for (const suggest of suggestions) { // eslint-disable-line no-restricted-syntax
    const [lat, lng] = await getCoordinates(`${DOMEN}/${encodeURI(suggest.namealt)}`);
    if (Math.abs(locationCoords[0] - lat) <= DELTA && Math.abs(locationCoords[1] - lng) <= DELTA) {
      result = decodeURI(suggest.namealt);
      break;
    }
  }

  return result;
}

function getPageAlias (location) {
  const { country: locationCountry } = normalizeLocation(location);
  const getAlias = asyncCompose(
    asyncPartialRight(filterByCoords, location.coords.slice()),
    asyncPartialRight(getFilteredSuggestions, locationCountry)
  );

  return getAlias(`${DOMEN}/jsonsearch.php?q=${encodeURI(location.location)}`);
}

function sleep (time) {
  return new Promise(resolve => setTimeout(resolve, time * 1000));
}

async function getArchiveTemp (alias) {
  const result = [];
  if (alias) {
    let currentYear = YEAR_FROM;
    while (currentYear <= YEAR_TO) {
      result.push({ year: currentYear, temp: await getLocationTemp(alias, currentYear) });
      await sleep(SLEEP_TIME);
      currentYear += 1;
    }
  }

  return result;
}

/**
 * Fetching weather data for each location from RP5.ru.
 * @return {Promise<void>}
 */
async function start () {
  const locations = await models.Location.find();

  const getData = asyncCompose(
    getArchiveTemp,
    getPageAlias
  );
  const transform = (acc, val, location) => {
    return acc.concat({
      locationId: location._id,
      name: location.location,
      vals: val
    });
  };
  try {
    const acc = await asyncReduce(getData, transform, [], locations);
    acc.forEach((result) => {
      if (result.vals.length) {
        result.vals.forEach(async t => await models.Weather({ temp: t.temp, year: t.year, locationId: result.locationId }).save());
      } else {
        log(result.name);
      }
    });
  } catch (error) { log(error); }
  // process.exit();
}

start();
