/* eslint-disable  import/no-extraneous-dependencies, no-await-in-loop, no-console, no-constant-condition */
export async function asyncReduce (fn, transform, acc, list) {
  for (let i = 0; i < list.length; i += 1) {
    const val = await fn(list[i]);
    acc = transform(acc, val, list[i]);
  }
  return acc;
}

export function asyncCompose (...fns) {
  return function innerAsyncCompose (...args) {
    const copy = [].concat(fns);
    return (async function next (...arg) { // eslint-disable-line wrap-iife
      const fn = copy.pop();
      const r = await fn(...arg);
      return copy.length ? next(r) : r;
    })(...args);
  };
}

export function asyncPartialRight (fn, ...args) {
  return async function innerAsyncPartial (...nextArgs) {
    return await fn(...nextArgs, ...args); // eslint-disable-line no-return-await
  };
}

export function normalizeLocation (location) {
  const reLoc = /^(поселок городского типа)|(рабочий поселок)|поселок|село|деревня|город|остров\s+/i;
  return {
    location: location.location.replace(reLoc, '').trim(),
    country: location.country.replace(/Соединённые Штаты Америки/i, 'США')
  };
}
