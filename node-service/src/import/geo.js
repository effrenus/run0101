import fs from 'fs';
import path from 'path';
import { models } from 'mongoose';
import '../server/db';

const fixtures = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../fixtures/points.json')).toString());

Object.keys(fixtures).forEach((key) => {
  const { coords, locality: { country, name, code } } = fixtures[key];
  (new models.Location({
    country,
    location: name,
    countryCode: code,
    coords
  })).save();
});
