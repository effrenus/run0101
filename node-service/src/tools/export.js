const fs = require('fs');
const crypto = require('crypto');
const got = require('got');
// const config = require('../config');

const geocodeCache = JSON.parse(fs.readFileSync('./gg.json').toString());
// const participants = JSON.parse(fs.readFileSync('./data/participants.json').toString());
const geoPoints = {};

function fetchWeather (key) {
  return got(`http://api.apixu.com/v1/history.json?key=${config.WEATHER_API_KEI}&q=${geoPoints[key].coords[0]},${geoPoints[key].coords[1]}&dt=2017-01-01`, { json: true })
    .then((res) => {
      geoPoints[key].weather = res.body.forecast && res.body.forecast.forecastday && res.body.forecast.forecastday[0] ? {
        temp: res.body.forecast.forecastday[0].day.avgtemp_c,
        icon: res.body.forecast.forecastday[0].day.condition.icon.replace('//cdn.apixu.com/weather', '')
      } : null;
    });
}

function getGeoJSON () {
  return JSON.stringify({
    type: 'FeatureCollection',
    features: Object.keys(geoPoints).map(key => ({
      type: 'Feature',
      id: key,
      geometry: {
        type: 'Point', coordinates: geoPoints[key].coords
      },
      properties: {
        alias: geoPoints[key].alias,
        weather: geoPoints[key].weather,
        locality: geoPoints[key].locality
        // gender: [
        //   participants.filter(p => p.geopointHash === key && p.gender === 0).length,
        //   participants.filter(p => p.geopointHash === key && p.gender === 1).length,
        //   participants.filter(p => p.geopointHash === key && typeof p.gender === 'undefined').length
        // ]
      }
    }))
  });
}

// module.exports = function () {
Object.keys(geocodeCache).forEach((key) => {
  if (!geocodeCache[key].coords) {
    return;
  }
  const coords = geocodeCache[key].coords;
  const hash = crypto.createHash('md5').update(`${coords[0]}${coords[1]}`).digest('hex');

  if (!geoPoints[hash]) {
    geoPoints[hash] = {
      coords,
      alias: [geocodeCache[key].loc],
      locality: geocodeCache[key].locality
    };
  } else if (geoPoints[hash].alias.indexOf(geocodeCache[key].loc) === -1) {
    geoPoints[hash].alias.push(geocodeCache[key].loc);
  }
});

console.log(JSON.stringify(geoPoints));

// return Promise.resolve() // Promise.all(Object.keys(geoPoints).map(key => fetchWeather(key)))
//   .then(geoPoints);
// };
