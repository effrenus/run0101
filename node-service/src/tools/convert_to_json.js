const fs = require('fs');
const csv = require('csv')

const dump = fs.readFileSync('./dump.csv').toString();
const csvFields = ['id', 'name', 'surname', 'email', 'sex', 'km', 'activity', 'country', 'city', 'comment', 'year', 'groupId', 'creationDate', 'ip', 'sessionHash', 'isValid'];

csv.parse(dump, (err, output) => {
  console.log(
    JSON.stringify(
      output.map(row => row.reduce((prev, cur, i) => { prev[csvFields[i]] = cur; return prev}, {}))
    )
  );
});
