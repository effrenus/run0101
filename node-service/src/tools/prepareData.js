'use strict';

const fs = require('fs'),
	crypto = require('crypto'),
	got = require('got'),
	SexByRussianName = require('./gender_guess'),
	fetchData = require('./fetchRawData'),
	config = require('../config');

const geocodeCache = JSON.parse(fs.readFileSync('../data/geocache.json').toString());

function geocodeYandex (address, loc, hashKey) {
	return got(`https://geocode-maps.yandex.ru/1.x/?geocode=${address}&format=json&kind=locality&results=1&key=${config.KEY.YANDEX_API}`, {json: true})
		.then(response => {
			try {
				const geoObject = response.body.response.GeoObjectCollection.featureMember[0].GeoObject;
				geocodeCache[hashKey] = {
					coords: geoObject.Point.pos.split(' ').reverse().map(p => parseFloat(p)),
					loc: loc,
					locality: {
						country: geoObject.metaDataProperty.GeocoderMetaData.AddressDetails.Country.CountryName,
						code: geoObject.metaDataProperty.GeocoderMetaData.AddressDetails.Country.CountryNameCode,
						name: geoObject.name
					}
				};
			} catch (err) {
				return geocodeGoogle(address, loc, hashKey);
			}

			return geocodeCache[hashKey].coords;
		});
}

function geocodeGoogle (address, loc, hashKey) {
	return got(`https://maps.googleapis.com/maps/api/geocode/json?address=${address}&language=ru&key=${config.KEY.GOOGLE_API}`, {json: true})
		.then(response => {
			if (response.body.results && response.body.results[0]) {
				geocodeCache[hashKey] = {
					coords: [response.body.results[0].geometry.location.lat, response.body.results[0].geometry.location.lng],
					loc: loc,
				};
			} else {
				geocodeCache[hashKey] = {coords: '', loc: loc};
			}

			return geocodeCache[hashKey].coords;
		});
}

function isLatin (word) {
	return (word.charCodeAt(0) >= 65 && word.charCodeAt(0) <= 90) || (word.charCodeAt(0) >= 97 && word.charCodeAt(0) <= 122)
}

function geocode (country, city) {
	const hashKey = crypto.createHash('md5').update(`${country},${city}`).digest('hex');
	const address = encodeURIComponent(country + ',' + city);
	const loc = `${country},${city}`;

	return geocodeCache.hasOwnProperty(hashKey) ? Promise.resolve(geocodeCache[hashKey].coords) : geocodeYandex(address, loc, hashKey);
}

function retrieveGender (fio) {
	const fioParts = fio.split(' ').map(i => i.trim());

	if (isLatin(fioParts[0])) {
		// return Promise.resolve(-1);
		return got(`https://api.genderize.io/?name=${encodeURI(fioParts[0])}`, {json: true})
			.then(res => {
				if (res.body.gender !== null && res.body.probability >= 0.7) {
					return res.body.gender === 'female' ? 0 : 1;
				} else {
					return undefined;
				}
			});
	} else {
		let genderType = (new SexByRussianName(fioParts[0], fioParts[1] && fioParts[1].indexOf('.') === -1 ? fioParts[1] : '', fioParts[2] ? fioParts[2] : '')).get_gender();

		return Promise.resolve(genderType !== undefined ? genderType : (new SexByRussianName(fioParts[1] ? fioParts[1] : '', fioParts[0], '')).get_gender());
	}

}

module.exports = function () {
	return fetchData(config.url)
		.then(rawData => {
			return new Promise((resolve, reject) => {
				const result = [];

				// Process data by chunks
				(function process () {
					Promise.all(rawData.splice(0, 100).map(data => {
						return geocode(data.country.trim(), data.city.trim().split(',')[0])
							.then(coordinates => {
								return {
									number: data.number,
									runner: data.runner.trim(),
									distance: parseInt(data.km.split(' ')[0]),
									activityType: data.activity === 'бегом' ? 1 : 2,
									geopointHash: crypto.createHash('md5').update('' + coordinates[0] + coordinates[1]).digest('hex'),
									// originalLocation: data.country.trim() + ', ' + data.city.trim().split(',')[0],
									comment: data.comment
								};
							})
							.then(data => {
								return retrieveGender(data.runner)
									.then(genderType => {
										data.gender = genderType;
										result.push(data);
									});
							})
							.catch(err => { console.log(err); });
					}))
						.then(() => {
							if (rawData.length) {
								// Set timeout cause google geocode time rate limit
								setTimeout(process, 1000);
							} else {
								resolve({
									geocodeCache: geocodeCache,
									participants: result
								});
							}
						})
				})();
			});
		});
};
