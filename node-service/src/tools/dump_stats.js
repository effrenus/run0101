const fs = require('fs');

const dump = JSON.parse(fs.readFileSync('./dump.json').toString());
const users = new Map();
const email_domain = new Map();

dump.forEach(u => {
  if (users.has(u.email)) {
    users.get(u.email).push(u.name + ' - ' + u.year);
  } else {
    users.set(u.email, [u.name + ' - ' + u.year]);
  }
});

users.forEach((v, k) => {
  const emailDomain = k && k.split('@')[1] ? k.split('@')[1].toLowerCase() : 'undef';
  (email_domain.has(emailDomain) ? email_domain.set(emailDomain, email_domain.get(emailDomain) + 1) : email_domain.set(emailDomain, 1));
});
console.log(email_domain);
