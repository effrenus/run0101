const fs = require('fs'),
  crypto = require('crypto'),
  got = require('got'),
  path = require('path');

const YANDEX_API = 'AGqqI1ABAAAA-V4IVgIA9p0m8nXZcRvKbkjJBG2t_Oy7tEkAAAAAAAAAAABN8n6nKhJ95QvnCdcGtgO4dJx7cA==',
    GOOGLE_API = 'AIzaSyATKaworTltMn4-K64tIyX0JTH39BLfZlg';

const geocodeCache = {};
const rawData = JSON.parse(
  fs.readFileSync(path.resolve(__dirname, '../fixtures/dump.json')).toString()
);

function geocodeYandex (address) {
  return got(`https://geocode-maps.yandex.ru/1.x/?geocode=${address}&format=json&kind=locality&results=1&key=${YANDEX_API}`, {json: true})
    .then(response => {
      try {
        const geoObject = response.body.response.GeoObjectCollection.featureMember[0].GeoObject;
        return {
          coords: geoObject.Point.pos.split(' ').reverse().map(p => parseFloat(p)),
          locality: {
            country: geoObject.metaDataProperty.GeocoderMetaData.AddressDetails.Country.CountryName,
            code: geoObject.metaDataProperty.GeocoderMetaData.AddressDetails.Country.CountryNameCode,
            name: geoObject.name
          }
        };
      } catch (err) {
        return geocodeGoogle(address);
      }
    });
}

function geocodeGoogle (address) {
  return got(`https://maps.googleapis.com/maps/api/geocode/json?address=${address}&language=ru&key=${GOOGLE_API}`, {json: true})
    .then(response => {
      if (response.body.results && response.body.results[0]) {
        return {
          coords: [response.body.results[0].geometry.location.lat, response.body.results[0].geometry.location.lng],
          locality: '',
        };
      } else {
        return {coords: null, locality: null};
      }
    });
}

function geocode (address) {
  return geocodeYandex(address);
}

(function process () {
  Promise.all(rawData.splice(0, 100).map(data => {
    const {country, city} = data;
    const userLocality = `${data.country.trim()},${data.city.trim().split(',')[0]}`.toLowerCase();
    const hashKey = crypto.createHash('md5').update(userLocality).digest('hex');

    return geocodeCache.hasOwnProperty(hashKey) ? Promise.resolve() : geocode(encodeURIComponent(country + ',' + city)).then(({coords, locality}) => {
      geocodeCache[hashKey] = {
        coords,
        userLocality,
        locality
      }
    })
  }))
    .then(() => {
      if (rawData.length) {
        setTimeout(process, 1000);
      } else {
        console.log(JSON.stringify(geocodeCache));
      }
    })
})();
