'use strict';

// const prepareData = require('./prepareData'),
// 	exportGeoPoints = require('./export');

// prepareData().then(res => console.log(JSON.stringify(res.participants))).catch(err => console.log(err));
// exportGeoPoints().then(geoJson => JSON.parse(geoJson).features.forEach(d => console.log(`${d.properties.locality.country}, ${d.properties.locality.name}`,  '=>\n', `${d.properties.alias.map(d => '\t'+d).join('\n')}\n`))).catch(err => console.log(err))

const fs = require('fs');

const data = JSON.parse(fs.readFileSync('../data/participants.json').toString());
const geo = JSON.parse(fs.readFileSync('../data/points.geojson').toString());

function getCat (t) {
	return Math.floor(t/5);
}

const stat = {};
geo.features.forEach(g => {
	const t = g.properties.weather.temp;
	const cat = getCat(t);
	const dist = data.filter(d => d.geopointHash === g.id).reduce((prev, cur) => prev += 1, 0);

	stat[cat] = stat[cat] ? stat[cat] + dist : dist;
});

Object.keys(stat).forEach(s => {
	s = parseInt(s);
	const sign = s >= 0 ? 1 : -1;
	const start = s >= 0 ? s * 5 : (s + 1) * 5;
	const end = start + sign * 5;
	console.log([start, end], stat[s])
})

// const r = {};
// data.forEach(d => {
// 	if (d.activityType === 2) {
// 		return;
// 	}
// 	const dist = d.distance;
// 	if (r[dist]) {
// 		r[dist] += 1;
// 	} else {
// 		r[dist] = 1;
// 	}
// });

// Object.keys(r).forEach(k => console.log(`${k},${r[k]}`));