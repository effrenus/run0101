const fs = require('fs'),
	got = require('got'),
	cheerio = require('cheerio');

function collectData(html) {
	const $ = cheerio.load(html);
	const data = [];

	$('.results tr').each((i ,el) => {
		const $row = $(el);

		data.push({
			number: $row.find('.number').text(),
			runner: $row.find('.runner').text(),
			km: $row.find('.km').text(),
			activity: $row.find('.activity').text(),
			country: $row.find('.country').text(),
			city: $row.find('.city').text(),
			comment: $row.find('.comment').text()
		});
	});

	return data;
}

module.exports = function (url) {
	return got(url)
		.then(response => response.body)
		.then(collectData);
}