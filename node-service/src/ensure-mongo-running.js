const mongoose = require('mongoose');
mongoose.Promise = Promise;

const retryInterval = 3000;
const maxRetry = 6;

let connectionAttempts = 0;

function tryToConnect() {
  connectionAttempts += 1;

  if (connectionAttempts > maxRetry) {
    process.exit(1);
  }

  mongoose.connect(process.env.MONGODB_URI, { useMongoClient: true })
    .then(() => {
      mongoose.disconnect();
      process.exit(0);
    })
    .catch(() => setTimeout(tryToConnect, retryInterval));
}

tryToConnect();
