/* global self */
var CACHE_NAME = 'run0101_v9';
var STATIC_PATH = '/static';

var urlsToCache = [
  '/api/v1/2014/data/json/',
  '/api/v1/2015/data/json/',
  '/api/v1/2016/data/json/',
  '/api/v1/2017/data/json/',
  STATIC_PATH + '/js/heatmap.min.js'
];

self.addEventListener('activate', function (event) {
  event.waitUntil(
    caches.keys().then(function (cacheNames) {
      return Promise.all(
        cacheNames.map(function(cacheName) {
          if (cacheName !== CACHE_NAME) {
            return caches.delete(cacheName);
          }
        })
      );
    })
  );
});

self.addEventListener('install', function (event) {
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(function (cache) {
        return fetch(STATIC_PATH + '/dist/manifest.json')
          .then(function (response) {
            return response.json();
          })
          .then(function (manifest) {
            return cache.addAll(
              urlsToCache
                .concat(
                  Object.keys(manifest)
                    .map(function (k) {
                      return STATIC_PATH + '/dist/' + manifest[k]
                    })
                )
            );
          });
      })
  );
});

self.addEventListener('fetch', function (event) {
  event.respondWith(
    caches.match(event.request)
      .then(function(response) {
        if (response) {
          return response;
        }
        return fetch(event.request);
      }
    )
  );
});

self.addEventListener('push', function (event) {
  const title = (event.data && event.data.text()) || '';
  const body = 'We have received a push message';
  const icon = STATIC_PATH + '/homer.png';

  event.waitUntil(
    self.registration.showNotification(title, { body, icon })
  );
});
