/* eslint no-use-before-define: 0 */
import mongoose from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';

const Schema = mongoose.Schema;

const participantSchema = new Schema({
  firstname: {
    type: Schema.Types.String,
    required: true
  },
  lastname: {
    type: Schema.Types.String,
    required: true
  },
  email: {
    type: Schema.Types.String,
    required: true
  },
  gender: Schema.Types.Number,
  created_at: Schema.Types.Date,
});

if (!participantSchema.options.toJSON) {
  participantSchema.options.toJSON = {};
}
participantSchema.options.toJSON.transform = (_, ret) => {
  ret.id = ret._id;
  delete ret._id;
  delete ret.__v;
};


participantSchema.statics.getAll = () => Participant.find().select('firstname lastname gender');

participantSchema.statics.getById = id => Participant.findOne({ _id: id }).select('firstname lastname gender');

participantSchema.plugin(autoIncrement.plugin, 'Participant');

const Participant = mongoose.model('Participant', participantSchema);

export default Participant;
