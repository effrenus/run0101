/* eslint no-use-before-define: 0 */
import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const activityAnnounceSchema = new Schema({
  name: {
    type: Schema.Types.String,
    required: true
  },
  announce_url: {
    type: Schema.Types.String,
    required: true
  },
  announce_date: {
    type: Schema.Types.Date,
    required: true
  }
});

const ActivityAnnounce = mongoose.model('ActivityAnnounce', activityAnnounceSchema);

export default ActivityAnnounce;
