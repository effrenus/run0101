/* eslint no-use-before-define: 0 */
import mongoose from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';

const Schema = mongoose.Schema;

const activitySchema = new Schema({
  type: {
    type: Schema.Types.Number,
    required: true
  },
  distance: {
    type: Schema.Types.Number,
    required: true
  },
  comment: Schema.Types.String,
  trackUrl: Schema.Types.String,
  created_at: {
    type: Schema.Types.Date,
    default: Date.now()
  },
  modified_at: Schema.Types.Date,
  participantId: {
    type: Schema.Types.Number,
    ref: 'Participant',
    required: true
  },
  locationId: {
    type: Schema.Types.Number,
    ref: 'Location'
  },
  announceId: {
    type: Schema.Types.ObjectId,
    ref: 'ActivityAnnounce'
  }
});

if (!activitySchema.options.toJSON) {
  activitySchema.options.toJSON = {};
}
activitySchema.options.toJSON.transform = (_, ret) => {
  ret.id = ret._id;
  delete ret._id;
  delete ret.__v;
};

activitySchema.statics.getByYear = year =>
  Activity
    .find({ created_at: { $gte: new Date(year, 0), $lt: new Date(year, 0, 4) } })
    .sort({ created_at: 1 })
    .select('type distance comment trackUrl participantId locationId');

activitySchema.statics.getById = id => Activity.findOne({ _id: id });

activitySchema.plugin(autoIncrement.plugin, 'Activity');

const Activity = mongoose.model('Activity', activitySchema);

export default Activity;
