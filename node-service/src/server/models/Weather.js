/* eslint no-use-before-define: 0 */
import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const weatherSchema = new Schema({
  temp: Schema.Types.Number,
  year: Schema.Types.Number,
  locationId: {
    type: Schema.Types.Number,
    ref: 'Location'
  }
});

if (!weatherSchema.options.toJSON) {
  weatherSchema.options.toJSON = {};
}
weatherSchema.options.toJSON.transform = (_, ret) => {
  ret.id = ret._id;
  delete ret._id;
  delete ret.__v;
};

const Weather = mongoose.model('Weather', weatherSchema);

export default Weather;
