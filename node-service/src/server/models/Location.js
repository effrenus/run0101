/* eslint no-use-before-define: 0 */
import mongoose from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';

const Schema = mongoose.Schema;

const locationSchema = new Schema({
  country: {
    type: Schema.Types.String,
    required: true
  },
  location: {
    type: Schema.Types.String,
    required: true
  },
  countryCode: Schema.Types.String,
  coords: {
    type: [],
    required: true
  }
});

if (!locationSchema.options.toJSON) {
  locationSchema.options.toJSON = {};
}
locationSchema.options.toJSON.transform = (_, ret) => {
  ret.id = ret._id;
  delete ret._id;
  delete ret.__v;
};

locationSchema.statics.findByName = (country, location) =>
  Location
    .findOne({ country, location });

locationSchema.statics.getByIds = ids =>
  Location.find({ _id: { $in: ids } });

locationSchema.statics.getById = id =>
  Location.findOne({ _id: id });

locationSchema.statics.getAll = () => Location.find();

locationSchema.plugin(autoIncrement.plugin, 'Location');

const Location = mongoose.model('Location', locationSchema);

export default Location;
