/* eslint no-use-before-define: 0 */
import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const notificationSchema = new Schema({
  /**
   * @type {Object} { enpoint: string, keys: { p256dh: string, auth: string } }
   */
  subscription: {
    type: Schema.Types.Mixed,
    required: true,
    unique: true
  },
  subscriptions: Schema.Types.Mixed
});

if (!notificationSchema.options.toJSON) {
  notificationSchema.options.toJSON = {};
}
notificationSchema.options.toJSON.transform = (_, ret) => {
  ret.id = ret._id;
  delete ret._id;
  delete ret.__v;
};

const Notification = mongoose.model('Notification', notificationSchema);

export default Notification;
