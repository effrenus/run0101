module.exports = {
  policy: {
    directives: {
      'default-src': ['self', 'api-maps.yandex.ru', '*.maps.yandex.net', '*.googleapis.com', '*.google-analytics.com', 'fonts.gstatic.com', '*.strava.com', '*.dropboxusercontent.com'],
      'style-src': ['self', 'unsafe-inline', 'blob:', 'https://fonts.googleapis.com'],
      'script-src': ['self', 'unsafe-inline', 'unsafe-eval', '*.google-analytics.com', 'https://graph.facebook.com/', 'https://vk.com/', 'https://connect.ok.ru', ' https://api-maps.yandex.ru', 'https://*.maps.yandex.net', 'https://yandex.ru'],
      'frame-src': ['https://api-maps.yandex.ru'],
      'img-src': ['self', 'data:', '*.cdninstagram.com', '*.google-analytics.com', 'https://*.maps.yandex.net', 'https://yandex.ru', 'https://api-maps.yandex.ru'],
      'child-src': ['self', 'https://api-maps.yandex.ru'],
      'report-uri': 'https://fb5f3526454f926320624dbb6eb5b195.report-uri.io/r/default/csp/enforce'
    }
  }
};
