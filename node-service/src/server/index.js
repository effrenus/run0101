import 'babel-polyfill';

import path from 'path';
import fs from 'fs';
import express from 'express';
import locale from 'locale';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import debug from 'debug';
import cors from 'cors';
import mongoose from 'mongoose';
import { SUPPORT_LOCALES } from '../config';
import routes from './routes';

import './db';

const app = express();
const log = debug('app');

app.set('env', process.env.NODE_ENV || 'production');
app.set('host', process.env.HOST || '127.0.0.1');
app.set('port', parseInt(process.env.PORT, 10) || 8000);

app.use(bodyParser.json());
app.use(cookieParser());

locale.Locale.default = SUPPORT_LOCALES[0];
app.use(locale(SUPPORT_LOCALES));
app.use(require('./middleware/locale').default);

app.set('views', path.resolve(__dirname, './templates'));
app.set('view engine', 'pug');

app.get('/sw.js$', (req, res) => {
  res.set('Content-Type', 'application/javascript');
  res.status(200).send(
    fs.readFileSync(path.resolve(__dirname, './sw.js'))
  );
});

app.use('/api/v1/webpush', cors({ origin: process.env.CORS_ORIGIN || '*' }), routes.webpush);
app.use('/api/v1', cors({ origin: process.env.CORS_ORIGIN || '*' }), routes.api);
app.use('/', routes.pages);

app.use((req, res) => {
  res.redirect('/error/404/');
});

const server = app.listen(app.get('port'), () => {
  log('Server is listening...');
  process.send('ready');
});

function gracefulShutdown() {
  server.close(() => log('Server stopped listening.'));
  mongoose.disconnect();
}

process.on('SIGINT', () => {
  log('Shutdown server...');
  gracefulShutdown();
  process.exit(0);
});

process.on('uncaughtError', (error) => {
  log(`Uncaught error: ${error}`);
  gracefulShutdown();
  process.exit(1);
});
