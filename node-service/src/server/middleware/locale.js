import { SUPPORT_LOCALES } from '../../config';

export default function (req, res, next) {
  const { locale } = req.cookies;
  if (locale && SUPPORT_LOCALES.includes(locale)) {
    req.locale = req.cookies.locale;
  }
  next();
}
