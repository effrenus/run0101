import mongoose from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';
import debug from 'debug';
import { DB as config } from '../config';

const log = debug('db');

mongoose.connection.on('error', error => log(error));
const connection = mongoose.connect(config.uri, config.options);
mongoose.Promise = global.Promise;

autoIncrement.initialize(connection);

require('./models');
