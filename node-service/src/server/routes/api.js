import express from 'express';
import { getActivityData, getYearAllData } from './helpers';

const router = express.Router();

router.get(/\/activity\/(\d+)\/json\//i, async (req, res) => {
  const activityId = parseInt(req.params['0'], 10);

  res.status(200).json(
    await getActivityData(activityId)
  );
});

router.get(/\/(\d{4})\/data\/json\//i, async (req, res) => {
  const year = parseInt(req.params['0'], 10);

  res.json(
    await getYearAllData(year)
  );
});

export default router;
