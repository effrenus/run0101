import { Router } from 'express';
import webpush from 'web-push';
import { PUBLIC_VAPID_KEY } from '../../config';

const PRIVATE_VAPID_KEY = 'vnhQgFLPVK9ZjiZAmgV-1i4EQQVq1jr0rJUyoWNxMAA';
const TTL = 24 * 60 * 60; // Message time to live in seconds.
const router = new Router();

router.post('/', (req, res) => {
  const subscription = req.body;

  webpush.sendNotification(
    subscription,
    'Hello',
    {
      TTL,
      vapidDetails: {
        subject: 'mailto:effrenus@gmail.com',
        publicKey: PUBLIC_VAPID_KEY,
        privateKey: PRIVATE_VAPID_KEY
      }
    }
  );

  res.send('ok');
});

export default router;
