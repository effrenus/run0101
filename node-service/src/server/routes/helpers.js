import path from 'path';
import fs from 'fs';
import R from 'ramda';
import { models } from 'mongoose';

function getFilenames (dir) {
  const names = [];
  if (fs.existsSync(dir)) {
    names.push(
      ...fs.readdirSync(dir)
    );
  }
  return names;
}

export async function getActivityData (activityId) {
  const activity = await models.Activity.getById(activityId);
  const athlete = await models.Participant.getById(activity.participantId);
  const location = activity.locationId ? await models.Location.getById(activity.locationId) : null;

  return {
    ...R.pick(['id', 'type', 'distance', 'comment', 'created_at'], activity.toJSON()),
    athlete,
    location
  };
}

export async function getYearAllData (year) {
  const activities = await models.Activity.getByYear(year);
  const locationIds = R.compose(R.uniq, R.filter(Boolean), R.map(activity => activity.locationId))(activities);
  const locations = await models.Location.getByIds(locationIds);
  const weathers = await models.Weather.find({ locationId: { $in: locationIds }, year });
  const locationsObject = R.reduce(
    (acc, location) => {
      acc[location.id] = R.pick(['country', 'location', 'coords'], location);
      const temp = R.find(R.propEq('locationId', parseInt(location.id, 10)), weathers);
      if (temp) {
        acc[location.id].weather = temp;
      }
      return acc;
    },
    {},
    locations
  );
  const athleteIds = R.compose(R.uniq, R.filter(Boolean), R.map(activity => activity.participantId))(activities);
  const athletes = await models.Participant.find({ _id: { $in: athleteIds } }).select('firstname lastname gender');

  const photos = R.compose(
    R.map(filename => `/static/photo/${year}/${filename}`),
    getFilenames
  )(path.resolve(__dirname, `../static/photo/${year}`));

  return {
    year,
    activities,
    photos,
    locations: locationsObject,
    athletes: athletes.reduce((result, athlete) => {
      result[athlete.id] = R.pick(['lastname', 'firstname', 'gender'], athlete);
      return result;
    }, {})
  };
}
