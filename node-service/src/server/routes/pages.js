import fs from 'fs';
import util from 'util';
import express from 'express';
import React from 'react';
import debug from 'debug';
import mc from 'mc';
import R from 'ramda';
import { renderToString } from 'react-dom/server';
import { match, RouterContext } from 'react-router';
import { Provider } from 'react-redux';
import { getActivityData, getYearAllData } from './helpers';
import createStore from '../../client/store';
import reactRoutes from '../../client/serverRoutes';
import LanguageProvider from '../../client/containers/LanguageProvider';
import { setActivitiesData, changeLocale, setActivity } from '../../client/actions';

const log = debug('server');
const router = express.Router();

const cacheClient = new mc.Client('memcached:11211');
cacheClient.connect(function () {
  log(`Connected to the memcache on host ${process.env.MEMCACHE_HOST}`);
});
cacheClient.getCacheValue = util.promisify(cacheClient.get).bind(cacheClient);
cacheClient.setCacheValue = util.promisify(cacheClient.set).bind(cacheClient);

const getParamYear = R.compose(R.partialRight(parseInt, [10]), R.prop('0'));

const getAssetsManifest = (() => {
  function get () {
    return fs.existsSync('./static/dist/manifest.json')
        ? JSON.parse(fs.readFileSync('./static/dist/manifest.json'))
        : {};
  }
  const assetsManifest = get();

  return process.env.NODE_ENV === 'production' ? () => assetsManifest : get;
})();

function matchComponents (location) {
  return new Promise((resolve, reject) => {
    match({ routes: reactRoutes, location }, (error, redirectLocation, renderProps) => {
      if (error) {
        reject(error);
      }

      resolve([renderProps, redirectLocation]);
    });
  });
}

function render (renderProps, locale, store) {
  return renderToString(
    <Provider store={store}>
      <LanguageProvider locale="ru">
        <RouterContext
          {...renderProps}
          createElement={(Component, props) => <Component locale={locale} {...props} />} />
      </LanguageProvider>
    </Provider>
  );
}

function handleRequest (fillStoreFn = () => {}) {
  return async (req, res) => {
    try {
      const cachedPage = await cacheClient.getCacheValue(`page_${req.url}`);

      res.status(200).type('html').end(cachedPage[`page_${req.url}`]);
    } catch (_cacheError) {
      log('========sss=========', _cacheError)
      try {
        const store = createStore();
        const [renderProps, redirectLocation] = await matchComponents(req.url);

        if (redirectLocation) {
          log('Redirecting to ', redirectLocation.pathname + redirectLocation.query);
          res.redirect(redirectLocation.pathname + redirectLocation.query);
        }

        await fillStoreFn(req, store);
        store.dispatch(changeLocale(req.locale));

        res.render(
          'index',
          {
            html: render(renderProps, req.locale, store),
            initialState: JSON.stringify(store.getState()),
            lang: req.locale,
            manifest: getAssetsManifest()
          },
          function (err, html) {
            cacheClient.setCacheValue(`page_${req.url}`, html, { exptime: 24 * 60 * 60 });

            res.status(200).type('html').end(html);
          }
        );
      } catch (error) {
        log(error);
        res.sendStatus(500);
      }
    }
  };
}

// Return archive page for 2008-2013 years.
router.get(/\/(2008|2009|2010|2011|2012|2013)\//, (req, res) => {
  const year = getParamYear(req.params);

  res.render(`archive/${year}`);
});

router.get(/\/activity\/(\d+)\//, handleRequest(async (req, store) => {
  const activityId = parseInt(req.params['0'], 10);

  store.dispatch(setActivity(
    await getActivityData(activityId)
  ));
}));

router.get(/\/(\d{4})\//, handleRequest(async (req, store) => {
  const year = getParamYear(req.params);
  const yearData = await getYearAllData(year);

  R.compose(store.dispatch, setActivitiesData)(yearData);
}));

router.get(['/', '/about/', '/error/*'], handleRequest());

export default router;
