import api from './api';
import pages from './pages';
import webpush from './webpush';

export default {
  api,
  pages,
  webpush
};
