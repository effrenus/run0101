const ExtractTextPlugin = require('extract-text-webpack-plugin');
const WebpackManifset = require('webpack-manifest-plugin');
const webpack = require('webpack');
const path = require('path');

const SRC_PATH = path.join(__dirname, '../client');

const cssLoader = [
  'css-loader?' + ['modules', 'localIdentName=[local]__[hash:base64:4]', 'importLoaders=1', 'sourceMap'].join('&')
].join('!');

module.exports = {
  entry: {
    vendor: ['recharts', 'ramda', 'react', 'react-dom', 'react-redux', 'prop-types', 'react-router', 'react-intl'],
    main: './client/index.js'
  },

  output: {
    filename: '[name].[chunkhash].js',
    chunkFilename: '[name].[chunkhash].js',
    path: path.resolve(__dirname, '../static/dist'),
    publicPath: '/static/dist/'
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader',
        include: path.resolve(__dirname, '../')
      },
      {
        // Transform our own .css files using PostCSS and CSS-modules
        test: /\.css$/,
        include: SRC_PATH,
        loader: ExtractTextPlugin.extract({
          use: cssLoader
        })
      },
      {
        // Do not transform vendor's CSS with CSS-modules
        test: /\.css$/,
        include: /node_modules/,
        loader: ExtractTextPlugin.extract({
          use: 'css-loader'
        })
      }
    ]
  },

  resolve: {
    modules: [SRC_PATH, 'node_modules'],
    extensions: ['.js', '.jsx', '.json']
  },

  plugins: [
    new webpack.NamedModulesPlugin(),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: Infinity
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'runtime'
    }),
    new ExtractTextPlugin({
      filename: 'default.css',
      allChunks: true
    }),
    new WebpackManifset()
  ]
};
