const path = require('path');
const fs = require('fs');
const webpack = require('webpack');

const nodeModules = fs.readdirSync('node_modules')
  .filter(function (x) {
    return ['.bin'].indexOf(x) === -1;
  })
  .reduce(function (prev, mod) {
    prev[mod] = `commonjs ${mod}`;
    return prev;
  }, {});

let plugins = [
  new webpack.DefinePlugin({
    'process.env.BROWSER': JSON.stringify(false)
  })
];

if (process.env.NODE_ENV === 'production') {
  plugins = plugins.concat([
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.AggressiveMergingPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        unused: true,
        dead_code: true,
        warnings: false,
        screw_ie8: true
      }
    }),
    new webpack.NoEmitOnErrorsPlugin()
  ]);
}

module.exports = {
    target: 'node',

    externals: nodeModules,

    node: {
      __dirname: false,
    },

    entry: {
      main: './server/index.js'
    },

    output: {
      filename: 'index.compiled.js',
      path: path.resolve(__dirname, '../server')
    },

    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          loader: 'babel-loader',
          exclude: /node_modules/
        }
      ]
    },

    resolve: {
      modules: [path.join(__dirname, '../server'), path.join(__dirname, '../client'), 'node_modules'],
      extensions: ['.js', '.jsx', '.json']
    },

    plugins: plugins
};
