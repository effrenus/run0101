const merge = require('webpack-merge');
const webpackCommon = require('./webpack.common');
const webpack = require('webpack');

module.exports = merge(webpackCommon, {
    entry: {
        main: [
            './client/index.js'
        ]
    },

    devtool: 'eval',

    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('develop'),
            'process.env.BROWSER': JSON.stringify(true)
        })
    ]
});
