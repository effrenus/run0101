// @flow
export const DB = {
  uri: process.env.MONGODB_URI || 'mongodb://127.0.0.1:27017/run0101',
  options: {
    useMongoClient: true
  }
};

export const NEXT_YEAR = (new Date()).getFullYear() + 1;

export const KEYS = {
  YANDEX_API: 'AGqqI1ABAAAA-V4IVgIA9p0m8nXZcRvKbkjJBG2t_Oy7tEkAAAAAAAAAAABN8n6nKhJ95QvnCdcGtgO4dJx7cA==',
  GOOGLE_API: 'AIzaSyATKaworTltMn4-K64tIyX0JTH39BLfZlg',
  WEATHER_API: 'c570952a61e7439eab5213512170201',
  INSTAGRAM_TOKEN: '4462630545.ba4c844.9eb1dac4a3a04d529da17dfb12c10f43'
};

export const SUPPORT_LOCALES = ['ru', 'en'];

export const DEFAULT_LANG = 'ru';

export const PUBLIC_VAPID_KEY = 'BOt88rpZBwIGM8kJwlLDK6Cu9qp1E8TQx5h2QRkvbBp-F4IpfufKKx2ArXZGR-ct2HEhkMr9JniL6PkT3remPLg';
