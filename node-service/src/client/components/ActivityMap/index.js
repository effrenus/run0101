import React, { Component } from 'react';
import MapController from '../../utils/map/MapController';
import loadMap from '../../utils/map';
import { createGeoJson } from '../../../utils';
import PreloaderIcon from '../PreloaderIcon';
import styles from './styles.css';

let mapController;

class ActivityMap extends Component {
  state: {
    loading: boolean
  };

  props: { // eslint-disable-line react/sort-comp
    locations: LocationsType,
    activities: ActivitiesType
  };

  constructor () { // eslint-disable-line react/sort-comp
    super();
    this.state = {
      loading: true
    };
  }

  componentDidMount () {
    if (!mapController && process.env.BROWSER) {
      loadMap()
        .then(() => {
          mapController = new MapController(window.ymaps);
          this._addToMap();
        })
        .catch(err => console.error(err));
    } else if (process.env.BROWSER) {
      this._addToMap();
    }
  }

  componentWillUnmount () {
    mapController.removeMap();
  }

  _addToMap () {
    const { locations, activities } = this.props;
    const locationIds = new Set(
      activities.map(activity => activity.locationId).filter(Boolean)
    );
    const geoJSON = createGeoJson(
      Array.from(locationIds.keys()).map(key => ({ id: key, ...locations[key] })),
      activities
    );

    mapController.displayData(geoJSON);

    this.setState({ loading: false });
  }

  render () {
    return (
      <div className={styles['map-container']}>
        {this.state.loading ? <PreloaderIcon /> : null}
        <div id="map" className={styles.map} />
      </div>
    );
  }
}

export default ActivityMap;
