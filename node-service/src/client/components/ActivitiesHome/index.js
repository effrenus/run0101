// @flow
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import locale from '../../../i18n';
import ActivitiesStats from '../ActivitiesStats';
import BarChart from '../BarChart';
import type { ActivitiesType, ActivityType, LocationsType, WeatherType } from '../../reducers/activitiesData';
import { numberWithSpace } from '../../../utils';
import styles from './styles.css';

type DistanceStatsType = {
  key: number,
  val: number
};

function distanceStats (activities: ActivitiesType): Array<DistanceStatsType> {
  const res = {};
  activities.forEach((activity: ActivityType) => {
    const distance = Math.floor(activity.distance);
    res[distance] = res[distance] ? res[distance] + 1 : 1;
  });
  return Object.keys(res).map(key => ({ key: parseInt(key, 10), val: res[key] }));
}

class ActivitiesHome extends PureComponent {
  props: {
    activities: ActivitiesType,
    locations: LocationsType
  };

  getLocationsStats () {
    const { locations, activities } = this.props;
    const stats = [];
    Object.keys(locations).forEach((locationId) => {
      let distance = 0;
      const location = locations[locationId];
      activities.forEach((activity) => {
        if (activity.locationId == locationId) {
          distance += activity.distance;
        }
      });
      stats.push({
        id: locationId,
        country: location.country,
        location: location.location,
        distance
      });
    });

    return stats;
  }

  renderBarChart () {
    const chartData: Array<DistanceStatsType> = distanceStats(this.props.activities);

    return (
      <BarChart
        width={380} height={250} data={chartData} />
    );
  }

  renderTempBlock () {
    const { locations } = this.props;

    function getWeatherArray(locs): Array<?WeatherType> {
      return Object.keys(locs)
        .map(key => locs[key])
        .map(location => location.weather);
    }

    let weatherArray: Array<WeatherType> = getWeatherArray(locations).filter(Boolean);
    weatherArray = weatherArray.sort((a, b) => a.temp > b.temp ? 1 : -1); // eslint-disable-line no-confusing-arrow

    return weatherArray.length ? (
      <div className={styles.item}>
        <h2 className={styles.itemHead}><FormattedMessage id="acivities.title.temperature" /></h2>
        <span className={`${styles.tempBlock} ${styles.cold}`}>
          {weatherArray[0].temp}
          <span className={styles.tempLabel}>{locale.getMessage('acivities.temperature.label.min')}</span>
        </span>
        <span className={`${styles.tempBlock} ${styles.warm}`}>
          {weatherArray[weatherArray.length - 1].temp}
          <span className={styles.tempLabel}>{locale.getMessage('acivities.temperature.label.max')}</span>
        </span>
      </div>
    ) : '';
  }

  renderPlaceStats () {
    const { locations } = this.props;
    const countries = new Set(Object.keys(locations).map(key => locations[key].country));
    return (
      <div className={`${styles.item}`}>
        <h2 className={styles.itemHead}>Countries</h2>
        <div className={`${styles.itemContent} ${styles.small}`}>
          {Array.from(countries).map(country => <span>{country}, </span>)}
        </div>
      </div>
    );
  }

  render () {
    const { activities } = this.props;
    const runsCount = activities.length;
    const totalDistance = activities.reduce((res, activity) => res + activity.distance, 0);

    return (
      <section className={styles['section-stats']}>
        <div className={styles.row}>
          <div className={styles.item}>
            <h2 className={styles.itemHead}><FormattedMessage id="acivities.title.runcount" /></h2>
            {numberWithSpace(runsCount)}
          </div>
          <div className={styles.item}>
            <h2 className={styles.itemHead}><FormattedMessage id="acivities.title.distance" /></h2>
            {numberWithSpace(totalDistance)} <span className={styles.unit}>{locale.getMessage('unit.km')}</span>
          </div>
          {this.renderTempBlock()}
        </div>

        <div className={styles.row}>
          <div className={`${styles.item}`}>
            <h2 className={styles.itemHead}><FormattedMessage id="acivities.title.stats" /></h2>
            <ActivitiesStats stats={this.getLocationsStats()} />
          </div>

          {this.renderPlaceStats()}

          <div className={`${styles.item}`}>
            <h2 className={styles.itemHead}><FormattedMessage id="acivities.title.distancespread" /></h2>
            {this.renderBarChart()}
          </div>
        </div>
      </section>
    );
  }
}

export default ActivitiesHome;
