import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import locale from '../../../i18n';
import cx from 'classnames';
import styles from './styles.css';

const ARCHIVE_TO_YEAR = 2013;

class YearPicker extends Component {
  props: {
    theme: string,
    from: number,
    to: number,
    value?: number
  };

  state: { isOpen: boolean };

  static defaultProps = {
    theme: 'white',
    from: 2008,
    to: (new Date()).getFullYear() + 1
  };

  constructor () {
    super();
    this.state = { isOpen: false };
  }

  handleClick = () => this.setState(state => ({ isOpen: !state.isOpen }));

  render () {
    const { from, to, theme, value } = this.props;
    const defaultValue = locale.getMessage('yearpicker.year');

    return (
      <div
        onClick={this.handleClick}
        className={cx(styles['year-picker'], styles[`theme-${theme}`], { [styles.open]: this.state.isOpen })}>
        <span>{value || defaultValue}</span>
        <ul>
          {Array.from({ length: (to - from) + 1 })
            .map((_, index) => {
              const year = to - index;
              return (
                <li className={value && year == value ? styles.selected : ''}>
                  {year <= ARCHIVE_TO_YEAR || year == ((new Date()).getFullYear() + 1) ? <a rel="noopener noreferrer" target="_blank" href={`/${year}/`}>{year}</a> : <Link to={`/${year}/`}>{year}</Link>}
                </li>
              );
            })
          }
        </ul>
      </div>
    );
  }
}

export default YearPicker;
