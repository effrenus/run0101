import React from 'react';
import locale from '../../../i18n';
import styles from './styles.css';

const Home = () => (
  <div className={styles['header-container']}>
    <ul className={styles['header-moto']}>
      <li>
        <b>{locale.getMessage('moto.1.title')}</b>
        {locale.getMessage('moto.1.text')}
      </li>
      <li>
        <b>{locale.getMessage('moto.2.title')}</b>
        {locale.getMessage('moto.2.text')}
      </li>
      <li>
        <b>{locale.getMessage('moto.3.title')}</b>
        {locale.getMessage('moto.3.text')}
      </li>
    </ul>
  </div>
);

export default Home;
