import React, { Component } from 'react';

class InputFilter extends Component {
  props: {
    onChange: Function,
    placeholder?: string
  };

  state: {
    value: string
  };

  static defaultProps = {
    placeholder: '',
    onChange: () => {}
  };

  state = {
    value: ''
  };

  handleKeyPress = () => {
    this.setState(() => ({ value: this._input.value }));
    this.props.onChange(this._input.value);
  }

  render () {
    return (
      <div>
        <input
          type="text"
          ref={(ref) => { this._input = ref; }}
          onChange={this.handleKeyPress}
          value={this.state.value}
          placeholder={this.props.placeholder} />
      </div>
    );
  }
}

export default InputFilter;
