import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { notification } from '../../sw';
import styles from './styles.css';

class Reminder extends PureComponent {
  state: { // eslint-disable-line react/sort-comp
    isPermissionDenied: boolean,
    active: boolean
  };

  _switch: HTMLInputElement; // eslint-disable-line react/sort-comp

  static renderNotice = () => (<small className={styles.notice}><FormattedMessage id="reminder.notice" /></small>);

  constructor () {
    super();
    this.state = {
      isPermissionDenied: !notification.isNotificationGranted(),
      active: false
    };
  }

  componentWillMount () {
    notification.getSubscription().then(subscription => this.setState({ active: !!subscription }));
  }

  onSwitchChange = () => {
    if (notification.isNotificationGranted()) {
      if (this._switch.checked) {
        notification.subscribe();
      } else {
        notification.unsubscribe();
      }
      this.setState({ active: this._switch.checked });
    }
  }

  render () {
    return (
      <section className={styles.reminder}>
        <div className={styles.onoffswitch}>
          <input
            ref={(d) => { this._switch = d; }}
            checked={this.state.active}
            onChange={this.onSwitchChange}
            type="checkbox" id="myonoffswitch"
            className={styles['onoffswitch-checkbox']} />
          <label className={styles['onoffswitch-label']} htmlFor="myonoffswitch" />
        </div>
        <h3><FormattedMessage id="reminder.title" /></h3>
        {this.state.isPermissionDenied && Reminder.renderNotice()}
      </section>
    );
  }
}

export default Reminder;
