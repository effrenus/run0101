import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import type { LocationType, AthleteType } from '../../reducers/activitiesData';
import { fetchActivity } from '../../actions';
import loadData from '../../containers/LoadData';
import styles from './styles.css';

class AcitvityDetail extends PureComponent { // eslint-disable-line react/prefer-stateless-function
  props: {
    activity: {
      comment?: string,
      type: number,
      distance: number
    },
    athlete: AthleteType,
    location: LocationType
  };

  render () {
    const { activity } = this.props;
    const { athlete, location } = activity;

    return (
      <section className={styles['activity-container']}>
        <div className={`${styles['field']} ${styles['field-fio']}`}>{athlete.lastname} {athlete.firstname}</div>
        <div className={`${styles['field']} ${styles['field-distance']}`}>{activity.distance} км</div>
        <div className={`${styles['field']} ${styles['field-location']}`}>
          <svg width="25" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 1000">
            <path d="M754.6 526.3C503.6 989.1 500 990 500 990s-1.6-2.4-254.3-463.2c-43.1-54.3-69-122.6-69-197C176.7 153.1 321.5 10 500 10s323.3 143.1 323.3 319.7c-.1 74.2-25.8 142.3-68.7 196.6zM501.2 105.9c-125 0-226.3 100.2-226.3 223.8 0 123.6 101.3 223.8 226.3 223.8 125 0 226.3-100.2 226.3-223.8 0-123.5-101.3-223.8-226.3-223.8z"/>
          </svg>
          {location.country}, {location.location}
        </div>
        <div className={`${styles['field']} ${styles['field-year']}`}>{(new Date(activity.created_at)).getFullYear()}</div>
      </section>
    );
  }
}

const Container = loadData({
  loadData (props) {
    const activityId = parseInt(props.params.id, 10);
    props.dispatch(fetchActivity(activityId));
  },
  isDataAvailable (props) {
    const id = parseInt(props.params.id, 10);
    return props.activity && props.activity.id === id;
  }
})(AcitvityDetail);

export default connect(
  state => ({ activity: state.activity })
)(Container);
