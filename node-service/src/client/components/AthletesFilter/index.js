import React, { PureComponent } from 'react';
import { compose, partial } from 'ramda';
import InputFilter from '../InputFilter';
import SelectFilter from '../SelectFilter';
import locale from '../../../i18n';
import styles from './styles.css';

type FilterType = {
  [name: string]: ?Function
};

function filterBy (fieldName: string, compareValue: string | number, data: Array<any> | boolean) {
  if (typeof data === 'boolean') {
    return data;
  }

  let isFiltered = true;
  const [activity, athlete, location] = data;

  switch (fieldName) {
    case 'lastname':
    case 'firstname':
      isFiltered = athlete[fieldName].toLowerCase().indexOf(compareValue.toLowerCase()) !== -1;
      break;
    case 'distance':
      isFiltered = activity.distance == compareValue;
      break;
    case 'location':
      isFiltered = activity.locationId ? activity.locationId == compareValue : false;
      break;
    case 'country':
      isFiltered = activity.locationId ? location.country.toLowerCase().indexOf(compareValue.toLowerCase()) !== -1 : false;
      break;
    default:
      isFiltered = true;
  }

  return isFiltered ? [activity, athlete, location] : false;
}

function activitiesFilter (composeFilter) {
  return (activities, athletes, locations) => activities.filter((activity) => {
    const athlete = athletes[activity.participantId];
    const location = activity.locationId ? locations[activity.locationId] : null;

    return composeFilter([activity, athlete, location]);
  });
}

class AthletesFilter extends PureComponent {
  props: {
    onChange?: Function,
    locations: Object
  };

  static defaultProps = {
    onChange: () => {}
  };

  filter: FilterType = {};

  constructor () {
    super();
    this.handleLastnameChange = this.handleChange.bind(this, 'lastname', filterBy);
    this.handleFirstnameChange = this.handleChange.bind(this, 'firstname', filterBy);
    this.handleLocationChange = this.handleChange.bind(this, 'location', filterBy);
    this.handleCountryChange = this.handleChange.bind(this, 'country', filterBy);
  }

  getComposeFilter () {
    const filters = Object.keys(this.filter)
      .filter(key => this.filter[key])
      .map(key => this.filter[key]);
    return filters.length
      ? activitiesFilter(compose(result => (typeof result === 'boolean') ? result : true, ...filters))
      : null;
  }

  handleChange (field, filterFn, compareValue) {
    this.filter[field] = compareValue ? partial(filterFn, [field, compareValue]) : null;
    this.props.onChange(this.getComposeFilter());
  }

  render () {
    const { locations } = this.props;

    return (
      <div className={styles['activity-filter']}>
        <img className={styles['activity-filter-icon']} src="/static/images/filter-icon.svg" width={20} height={20} alt="" />
        <InputFilter placeholder={locale.getMessage('filter.label.lastname')} onChange={this.handleLastnameChange} />
        <InputFilter placeholder={locale.getMessage('filter.label.firstname')} onChange={this.handleFirstnameChange} />
        <SelectFilter
          placeholder={locale.getMessage('filter.label.location')}
          onChange={this.handleLocationChange}
          data={Object.keys(locations).map(key => ({ key, val: locations[key].location }))} />
        <SelectFilter
          placeholder={locale.getMessage('filter.label.country')}
          onChange={this.handleCountryChange}
          data={Array.from(Object.values(locations).reduce((acc, val) => {
            acc.add(val.country);
            return acc;
          }, new Set())).map(val => ({ key: val, val }))} />
      </div>
    );
  }
}

export default AthletesFilter;
