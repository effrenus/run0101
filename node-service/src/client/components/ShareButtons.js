import React, { PureComponent } from 'react';
import likely from 'ilyabirman-likely';

export default class SocialWidget extends PureComponent {
  componentDidMount () {
    if (process.env.BROWSER) {
      likely.initiate();
    }
  }

  render () {
    return (
      <section className="likely likely-big">
        <div className="facebook" />
        <div className="vkontakte" />
        <div className="odnoklassniki" />
        <div className="twitter" />
      </section>
    );
  }
}
