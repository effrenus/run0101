import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import localeInst from '../../../i18n';
import { changeLocale } from '../../actions';
import styles from './styles.css';

class LangSwitch extends PureComponent {
  props: {
    locale: string,
    dispatch: Dispatch
  };

  changeLocale = (event) => {
    const { locale } = event.target.dataset;
    document.cookie = `locale=${locale}`;
    localeInst.setLang(locale);
    this.props.dispatch(changeLocale(locale));
  }

  render () {
    const { locale } = this.props;
    return (
      <ul className={styles.switch} onClick={this.changeLocale}>
        <li className={`${styles.switchItem} ${locale === 'ru' ? styles.active : ''}`} data-locale="ru">ру</li>
        <li className={`${styles.switchItem} ${locale === 'en' ? styles.active : ''}`} data-locale="en">en</li>
      </ul>
    );
  }
}

export default connect(
  state => ({ locale: state.locale })
)(LangSwitch);
