import React, { PureComponent } from 'react';
import {
  BarChart,
  XAxis,
  YAxis,
  Bar
} from 'recharts';
import styles from './styles.css';

type DistanceStatsType = {
  key: number,
  val: number
};

class Chart extends PureComponent {
  props: {
    data: Array<DistanceStatsType>,
    width?: number,
    height?: number
  };

  static defaultProps = {
    width: 300,
    height: 200
  };

  state = {
    isFullscreen: false
  };

  handleFullscreen = () => {
    this.setState(state => ({ isFullscreen: !state.isFullscreen }));
  }

  render () {
    const { data, width, height } = this.props;
    const chartWidth = this.state.isFullscreen ? window.innerWidth * 0.9 : width;
    const chartHeight = this.state.isFullscreen ? window.innerHeight * 0.6 : height;

    return (
      <div style={{ width, height }}>
        <div
          className={this.state.isFullscreen ? styles.fullscreen : styles.base}>
          <BarChart
            width={chartWidth} height={chartHeight} data={data}
            margin={{ top: 0, right: 0, left: -30, bottom: 0 }}>
            <XAxis dataKey="key" />
            <YAxis dataKey="val" />
            <Bar dataKey="val" fill="#145493" />
          </BarChart>
          <button className={styles.fullBtn} onClick={this.handleFullscreen}>
            <img src={`/static/images/fullscreen-${this.state.isFullscreen ? 'close' : 'open'}.svg`} width={30} height={30} alt="" />
          </button>
        </div>
      </div>
    );
  }
}

export default Chart;
