import React from 'react';
import styles from './styles.css';

const PreloaderIcon = () =>
  <div className={styles['preload-icon']} />;

export default PreloaderIcon;
