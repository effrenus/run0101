import React, { PureComponent } from 'react';
// import React from 'react';
import { FormattedMessage } from 'react-intl';
import Reminder from '../Reminder';
import Countdown from '../Countdown';
import styles from './styles.css';

class FutureActivities extends PureComponent { // eslint-disable-line  react/prefer-stateless-function
  props: {
    year: number
  };

  render () {
    const { year } = this.props;
    return (
      <div className={styles.invitation}>
        <h1><FormattedMessage id="countdown.title" /></h1>

        <span className={styles.time}>
          <Countdown year={year} />
        </span>

        <Reminder />
      </div>
    );
  }
}

export default FutureActivities;
