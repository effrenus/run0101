// @flow
import React from 'react';
import { FormattedMessage } from 'react-intl';
import styles from './styles.css';

const ActivitiesPhoto = (props: { photos: Array<string>, year: number }) => {
  const { photos, year } = props;
  return (
    <section className={styles.photoSection}>
      <div className={styles.moto}><FormattedMessage id="photo.moto" values={{ year }} /></div>
      <ul className={styles.list}>
        {photos.map(path => <li className={styles.item}><img src={path} alt="" /></li>)}
      </ul>
    </section>
  );
};

export default ActivitiesPhoto;
