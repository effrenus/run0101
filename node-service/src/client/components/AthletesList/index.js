import React, { Component } from 'react';
import { Link } from 'react-router';
import { FormattedMessage } from 'react-intl';
import type { ActivitiesType, AthletesType, LocationsType } from '../../reducers/activitiesData';
import AthletesFilter from '../AthletesFilter';
import styles from './styles.css';

class AthletesList extends Component {
  state: {
    limit: number,
    filterFn: Function
  };

  props: {
    athletes: AthletesType,
    activities: ActivitiesType,
    locations: LocationsType
  };

  constructor () { // eslint-disable-line react/sort-comp
    super();
    this.state = {
      limit: 100,
      filterFn: null
    };
  }

  onFilterChange = (filterFn) => {
    this.setState(() => ({ filterFn }));
  }

  showMore = () => this.setState(state => ({ limit: state.limit + 100 }));

  render () {
    const { athletes, activities, locations } = this.props;
    const { filterFn, limit } = this.state;
    const items = filterFn ? filterFn(activities, athletes, locations) : activities;

    return (
      <section className={styles.listSection}>
        <AthletesFilter onChange={this.onFilterChange} locations={locations} />
        <ul className={styles.list}>
          {items.slice(0, limit).map((activity, index) => {
            const athlete = athletes[activity.participantId];
            const location = activity.locationId ? locations[activity.locationId] : null;

            return (
              <li className={styles.listItem}>
                <Link to={`/activity/${activity.id}/`}>
                  <span className={`${styles.listField} ${styles.fieldIndex}`}>{index + 1}</span>
                  <span className={`${styles.listField} ${styles.fieldName}`}>{athlete.lastname} {athlete.firstname}</span>
                  <span className={`${styles.listField} ${styles.fieldDistance}`}><b>{activity.distance}</b> км</span>
                  <span className={`${styles.listField} ${styles.fieldLocation}`}>{location ? `${location.country}, ${location.location}` : null}</span>
                  <span className={`${styles.listField} ${styles.fieldComment}`}>{activity.comment}</span>
                </Link>
              </li>
            );
          })}
        </ul>
        {limit < items.length
          ? <button className={styles.morebutton} onClick={this.showMore}><FormattedMessage id="button.activities.more" values={{ count: Math.min(100, items.length - limit), whole: activities.length }} /></button>
          : null}
      </section>
    );
  }
}

export default AthletesList;
