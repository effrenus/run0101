import React from 'react';
import { FormattedMessage } from 'react-intl';
import styles from './styles.css';

const NotFound = () =>
  <section className={styles.notfound}>
    <h1 className={styles.title}><FormattedMessage id="error.notfound.title" /></h1>
    <div className={styles.ssmblock}>
      <a className={styles.ssmlink} href="https://www.facebook.com/groups/910839585652141/" target="_blank"><img src="/static/images/fb-icon.svg" width="60" alt="facebook" /></a>
      <a className={styles.ssmlink} href="https://vk.com/club1057312" target="_blank"><img src="/static/images/vk-icon.svg" width="60" alt="vk" /></a>
    </div>
  </section>

export default NotFound;
