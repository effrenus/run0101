// @flow
import React from 'react';
import { Link } from 'react-router';
import { FormattedMessage } from 'react-intl';
import LangSwitch from './LangSwitch';
import TopNavigation from './TopNavigation';
import YearPicker from './YearPicker';

import styles from '../default.css';

type PropsType = {
  router: Router,
  children: Class<React$Component<*, *, *>>
};

const App = (props: PropsType) => {
  const { pathname } = props.router.location;
  const isMainPage = pathname === '/';

  return (
    <section className={`${styles.page} ${isMainPage ? styles.main : styles.inner}`}>
      <header>
        <div className={styles.logo}>
          <h1>
            {isMainPage ? <FormattedMessage id="page.title" /> : <Link to="/"><FormattedMessage id="page.title" /></Link>}
          </h1>
          <YearPicker value={props.router.params.year} theme={isMainPage ? 'white' : 'black'} />
        </div>
        <TopNavigation params={props.router.params} isMainPage={isMainPage} />
        <LangSwitch />
      </header>

      {props.children}
    </section>
  );
};

export default App;
