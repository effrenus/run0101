import React, { Component } from 'react';
import locale from '../../../i18n';
import styles from './styles.css';

function isSelect (node) {
  let finded = false;
  while (node && !finded) {
    finded = node.className && node.className.includes('select');
    node = node.parentNode;
  }
  return finded;
}

class SelectFilter extends Component {
  props: {
    placeholder?: string,
    data: Array<{ key: string | number, val: string | number}>,
    onChange?: Function
  };

  state: {
    isActive: boolean,
    filterValue: string,
    currentValue?: string | number
  };

  static defaultProps = {
    placeholder: 'Select value',
    onChange: () => {}
  };

  state = {
    isActive: false,
    filterValue: '',
    currentValue: null
  };

  componentDidMount () {
    window.addEventListener('click', (event) => {
      if (!isSelect(event.target) && this.state.isActive) {
        this.setState(() => ({ isActive: false }));
      }
    });
  }

  componentWillUnmount () {
    //
  }

  toggle = () => {
    this.setState(state => ({ isActive: !state.isActive }));
  }

  select = (event) => {
    const value = event.currentTarget.dataset.key;
    if (value !== this.state.currentValue) {
      this.setState(() => ({ currentValue: value, isActive: false }));
      this.props.onChange(value);
    }
  }

  getName (value) {
    const { data } = this.props;

    for (let i = 0, len = data.length; i < len; i += 1) {
      if (data[i].key == value) {
        return data[i].val;
      }
    }
    return '';
  }

  handleFilterChange = () => {
    this.setState(() => ({ filterValue: this._filter.value }));
  }

  resetFilter = () => {
    this.setState(() => ({ currentValue: '', isActive: false }));
    this.props.onChange('');
  }

  render () {
    const { data, placeholder } = this.props;
    const { filterValue, isActive, currentValue } = this.state;
    const filteredData = filterValue ? data.filter(item => item.val.toLowerCase().indexOf(filterValue.toLowerCase()) !== -1) : data;

    return (
      <div ref={(ref) => { this._container = ref; }} className={`${styles['select-box']} ${isActive ? styles['active'] : ''}`}>
        <div onClick={this.toggle} className={styles['select-value']}>{ currentValue ? this.getName(currentValue) : placeholder }</div>

        <div className={styles['select-input']}>
          <input ref={(ref) => { this._filter = ref; ref && ref.focus(); }} onChange={this.handleFilterChange} value={this.state.filterValue} type="text" />
        </div>

        <div className={styles['select-list-container']}>
          { currentValue ? <button className={styles['reset-button']} onClick={this.resetFilter}>{locale.getMessage('button.filter.reset')}</button> : null}
          <ul className={styles['select-list']}>
            {filteredData.map(item =>
              <li key={item.key} onClick={this.select} data-key={item.key} className={styles['select-item']}>{item.val}</li>)}
          </ul>
        </div>

      </div>
    );
  }
}

export default SelectFilter;
