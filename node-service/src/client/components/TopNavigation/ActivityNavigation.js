import React, { PureComponent } from 'react';
import { Link, IndexLink } from 'react-router';
import { FormattedMessage } from 'react-intl';
import styles from './styles.css';

class ActivityNavigation extends PureComponent {
  state: {
    isOpen: boolean
  };

  state = {
    isOpen: false
  };

  toggleState = () => this.setState(state => ({ isOpen: !state.isOpen }));

  render () {
    const { isOpen } = this.state;
    const { year } = this.props;

    return (
      <div>
        <nav className={`${styles['nav-activity']} ${isOpen ? styles['isOpen'] : ''}`}>
          <ul>
            <li><IndexLink className={styles.link} activeClassName={styles.active} to={`/${year}/`}><FormattedMessage id="menu.top.stats" /></IndexLink></li>
            <li><Link className={styles.link} activeClassName={styles.active} to={`/${year}/map/`}><FormattedMessage id="menu.top.map" /></Link></li>
            <li><Link className={styles.link} activeClassName={styles.active} to={`/${year}/athletes/`}><FormattedMessage id="menu.top.user" /></Link></li>
            <li><Link className={styles.link} activeClassName={styles.active} to={`/${year}/photo/`}><FormattedMessage id="menu.top.photo" /></Link></li>
          </ul>
        </nav>
        <button
          onClick={this.toggleState}
          className={`${styles['nav-hamburger']} ${isOpen ? styles['active'] : ''}`}>
          <span></span>
        </button>
      </div>
    );
  }
}

export default ActivityNavigation;
