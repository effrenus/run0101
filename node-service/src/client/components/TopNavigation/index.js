import React, { PureComponent } from 'react';
import { Link, IndexLink } from 'react-router';
import { FormattedMessage } from 'react-intl';
import { NEXT_YEAR } from '../../../config';
import ActivityNavigation from './ActivityNavigation';
import styles from './styles.css';

class Nav extends PureComponent {
  props: { // eslint-disable-line react/sort-comp
    params: {
      year: string
    },
    isMainPage: boolean
  };

  static renderNav = year =>
    <ActivityNavigation year={year} />

  static renderMainNav = () =>
    <nav className={styles['nav-secondary']}>
      <ul>
        <li><Link className={styles.link} activeClassName={styles.active} to="/about/"><FormattedMessage id="menu.top.about" /></Link></li>
      </ul>
    </nav>

  render () {
    const year = this.props.params.year ? parseInt(this.props.params.year, 10) : null;

    return this.props.isMainPage // eslint-disable-line no-nested-ternary
      ? Nav.renderMainNav()
      : (year && year < NEXT_YEAR ? Nav.renderNav(year) : Nav.renderMainNav());
  }
}

export default Nav;
