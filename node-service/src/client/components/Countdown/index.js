import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';

const SECOND = 1000;
const MINUTE = 60 * SECOND;
const HOUR = 60 * MINUTE;
const DAY = 24 * HOUR;

class Countdown extends PureComponent {
  props: { // eslint-disable-line react/sort-comp
    year: number
  };

  state: {
    currentDate: Date,
    beginDate: Date
  };

  _timerId: number;

  constructor (props) { // eslint-disable-line react/sort-comp
    super();
    this.state = {
      currentDate: new Date(),
      beginDate: new Date(props.year, 0, 1)
    };
  }

  componentDidMount () {
    this._startTimer();
  }

  componentWillUnmount () {
    clearInterval(this._timerId);
  }

  _updateTime = () => {
    this.setState(prevState => ({
      currentDate: new Date(prevState.currentDate.getTime() + 1000)
    }));
  }

  _startTimer () {
    this._timerId = setInterval(this._updateTime, 1000);
  }

  render () {
    const { currentDate, beginDate } = this.state;
    let timeDiff = beginDate - currentDate;

    const timeParts = [DAY, HOUR, MINUTE, SECOND].map((i) => {
      const v = Math.floor(timeDiff / i);
      timeDiff -= v * i;
      return v;
    });

    return (<FormattedMessage
      id="countdown.time"
      values={{ days: timeParts[0], hours: timeParts[1], minutes: timeParts[2], seconds: timeParts[3] }} />);
  }
}

export default Countdown;
