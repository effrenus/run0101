import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import styles from './styles.css';

type StatType = {
  id: string,
  country: string,
  location: string,
  distance: number
};

const FILTER_TYPE = {
  COUNTRY: 1,
  LOCATION: 2
};

function statsSort (a: {distance: number}, b: {distance: number}) {
  return b.distance - a.distance;
}

class ActivitiesStats extends Component {
  props: {
    stats: Array<StatType>
  };

  state: {
    filterType: FILTER_TYPE.COUNTRY | FILTER_TYPE.LOCATION
  };

  state = {
    filterType: FILTER_TYPE.LOCATION
  };

  setFilterType = (event) => {
    event.stopPropagation();

    const type = parseInt(event.currentTarget.dataset.type, 10);
    if (this.state.filterType !== type) {
      this.setState(() => ({ filterType: type }));
    }
  }

  renderCountryStats () {
    const { stats } = this.props;
    const countryMapper = {};
    const countries = [];
    stats.forEach((stat) => {
      let index;
      if (typeof countryMapper[stat.country] !== 'undefined') {
        index = countryMapper[stat.country];
      } else {
        countries.push({
          country: stat.country,
          distance: 0
        });
        index = countryMapper[stat.country] = countries.length - 1;
      }
      countries[index].distance += stat.distance;
    });

    return (
      <ul className={styles['stats-list']}>
        {countries.sort(statsSort).map((stat, index) =>
          <li key={`country_${index}`} className={styles['stats-item']}>
            <span className={styles['stats-location']}>{stat.country}</span>
            <span className={styles['stats-distance']}>{stat.distance}</span>
          </li>)}
      </ul>
    );
  }

  renderLocationStats () {
    const { stats } = this.props;

    return (
      <ul className={styles['stats-list']}>
        {stats.sort(statsSort).map(stat =>
          <li key={stat.id} className={styles['stats-item']}>
            <span className={styles['stats-location']}>{stat.location}</span>
            <span className={styles['stats-distance']}>{stat.distance}</span>
          </li>)}
      </ul>
    );
  }

  render () {
    const { filterType } = this.state;

    return (
      <div className={styles['stats-container']}>
        <div className={styles['stats-head']}>
          <ul className={styles['stats-filter']}>
            <li
              className={filterType === FILTER_TYPE.LOCATION ? styles.active : ''}
              data-type={FILTER_TYPE.LOCATION}
              onClick={this.setFilterType}><FormattedMessage id="filter.by_city" /></li>
            <li
              className={filterType === FILTER_TYPE.COUNTRY ? styles.active : ''}
              data-type={FILTER_TYPE.COUNTRY}
              onClick={this.setFilterType}><FormattedMessage id="filter.by_country" /></li>
          </ul>
        </div>
        <article>
          {filterType === FILTER_TYPE.COUNTRY ? this.renderCountryStats() : this.renderLocationStats()}
        </article>
      </div>
    );
  }
}

export default ActivitiesStats;
