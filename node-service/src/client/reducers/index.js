// @flow
import { combineReducers } from 'redux';
import activitiesData from './activitiesData';
import locale from './locale';
import activity from './activity';

const rootReducer = combineReducers({
  activitiesData,
  locale,
  activity
});

export default rootReducer;
