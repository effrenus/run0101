// @flow
import { createReducer } from 'redux-act';
import { setActivity } from '../actions';

export default createReducer({
  // $FlowFixMe
  [setActivity]: (state: Object, payload: Object): Object => payload
}, null);
