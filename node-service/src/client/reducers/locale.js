// @flow
import { createReducer } from 'redux-act';
import { changeLocale } from '../actions';
import { DEFAULT_LANG } from '../../config';

export default createReducer({
  // $FlowFixMe
  [changeLocale]: (state: string, payload: string): string => payload
}, DEFAULT_LANG);
