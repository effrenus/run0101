// @flow
import { createReducer } from 'redux-act';
import { setActivitiesData } from '../actions';

export type WeatherType = {
  temp: number
};

export type LocationType = {
  id?: number,
  country: string,
  location: string,
  coords: [number, number],
  countryCode?: string,
  weather?: WeatherType
};

export type LocationsType = {
  [id: string]: LocationType
};

export type AthleteType = {
  lastname: string,
  firstname: string,
  gender: number
};

export type AthletesType = {
  [id: string]: AthleteType
};

export type ActivityType = {
  id: number,
  comment?: string,
  type: number,
  distance: number,
  locationId: number,
  participantId: number
};

export type ActivitiesType = Array<ActivityType>;

export type ActivitiesDataType = {
  year: number,
  athletes: AthletesType,
  activities: ActivitiesType,
  locations: LocationsType,
  photos: Array<string>
};

export default createReducer({
  // $FlowFixMe
  [setActivitiesData]: (state: ActivitiesDataType | {}, payload: ActivitiesDataType): ActivitiesDataType => payload
}, {});
