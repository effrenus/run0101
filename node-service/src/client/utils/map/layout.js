export const PIN_LAYOUT_NAME = 'run0101#pinLayout';

export function createPinLayout () {
  const CANVAS_STYLES = 'position:absolute; top:-5px; left:-5px;';

  const IconLayout = ymaps.templateLayoutFactory.createClass(`<canvas style="${CANVAS_STYLES}"></canvas>`,
    {
      build () {
        IconLayout.superclass.build.call(this);

        this.props = this.getData().properties;
        this.options = this.getData().options;
        this._canvas = this.getElement().querySelector('canvas');
        this._canvas.width = 10;
        this._canvas.height = 10;
        this._ctx = this._canvas.getContext('2d');

        this._setupCanvas();
        this.options.set({
          shape: this.options.get('shape', {
            type: 'Rectangle',
            coordinates: [
                [0, 0],
                [this._canvas.width, this._canvas.height]
            ]
          })
        });
      },

      _setupCanvas () {
        const ctx = this._ctx;
        ctx.fillStyle = '#145493';
        ctx.arc(5, 5, 5, 0, 2 * Math.PI);
        ctx.fill();
        ctx.lineWidth = 1;
        ctx.strokeStyle = '#FFF';
        ctx.stroke();
      }
    }
  );

  ymaps.layout.storage.add(PIN_LAYOUT_NAME, IconLayout);
}
