// @flow
import loadScript from '../../../utils/loadScript';
import locale from '../../../i18n';

function loadDeps (): Promise<void> {
  const mode = process.env.NODE_ENV === 'production' ? 'release' : 'debug';
  return loadScript(`https://api-maps.yandex.ru/2.1/?lang=${locale.getLang()}&csp=true&mode=${mode}`)
    .then(() => loadScript('/static/js/heatmap.min.js'));
}

let loadPromise;

const loadMap = (): Promise<void> =>
  loadPromise || (loadPromise = new Promise((resolve) => {
    if (process.env.BROWSER && !window.ymaps) {
      loadDeps()
        .then(() => ymaps.ready({ require: ['Heatmap'] }))
        .then(resolve);
    } else {
      resolve();
    }
  }));

export default loadMap;
