import { EventEmitter } from 'events';
import { createPinLayout, PIN_LAYOUT_NAME } from './layout';
import locale from '../../../i18n';

class MapController extends EventEmitter {
  constructor (ns, opts = {}) {
    super();
    this.ns = ns;
    this.options = {
      lang: 'ru',
      map: {
        center: [54.629283, 32.214976],
        controls: ['fullscreenControl'],
        zoom: 5
      },
      ...opts
    };
    this._setup();
    this._setupLayout();
    this._setupHeatmap();
    this._setupObjectManager();
  }

  _setupLayout () { // eslint-disable-line  class-methods-use-this
    createPinLayout();
    ymaps.option.presetStorage.add('run0101#Pin', { iconLayout: PIN_LAYOUT_NAME });
  }

  _setup () {
    this._setupMap();
    this._setupControls();
  }

  _setupMap () {
    const { ns, options } = this;

    const map = this.map = new ns.Map('map', options.map, { suppressMapOpenBlock: true }); // eslint-disable-line no-multi-assign

    map.controls.add('zoomControl', {
      size: 'small',
      position: { top: 'auto', bottom: 20, left: 10 }
    });

    map.events.add('balloonclose', evt => this.emit('balloonclose', evt));
  }

  _setupObjectManager (): void {
    const { ns, map } = this;
    const manager = this._objectManager = new ns.ObjectManager(); // eslint-disable-line no-multi-assign
    manager.objects.options.set('preset', 'run0101#Pin');
    manager.events.add('click', (evt) => {
      const targetMap = evt.originalEvent.currentTarget.getMap();
      const point = manager.objects.getById(evt.get('objectId'));
      const props = point.properties;

      targetMap.balloon.open(
        evt.get('coords'),
        {
          content: `
            <b>${props.location}</b>, <small>${props.country}</small><br>
            ${props.weather ? `<img height="18" class="map-icon" src="/static/images/weather.svg"> ${props.weather}℃` : ''}
            <img width="18" class="map-icon" src="/static/images/run.svg"> ${props.totalDistance} км
          `
        }
      );
      this.emit('pointClick', evt);
    });
    map.geoObjects.add(manager);
  }

  _setupHeatmap () {
    const { ns, map } = this;
    this._heatmap = new ns.Heatmap(null, {
      radius: 10,
      gradient: {
        0.05: 'rgba(50, 248, 255, 0.6)', 0.1: 'rgba(128, 255, 0, 0.55)', 0.25: 'rgba(251, 255, 135, 0.7)', // eslint-disable-line object-property-newline
        0.4: 'rgba(255, 255, 0, 0.8)', 0.55: 'rgba(255, 64, 0, 0.8)', 0.7: 'rgba(234, 72, 58, 0.9)', // eslint-disable-line object-property-newline
        0.85: 'rgba(162, 36, 25, 0.95)', 1.0: 'rgba(61, 2, 2, 1)' // eslint-disable-line object-property-newline
      }
    });
    this._heatmap.setMap(map);
  }

  _setupControls (): void {
    const { ns, map } = this;
    this._ctrls = {};

    const heatmapCtrl = this._ctrls.heatmapCtrl = new ns.control.Button({ // eslint-disable-line no-multi-assign
      options: { maxWidth: 200 },
      data: { content: locale.getMessages()['map.button.heatmap'] },
      state: { selected: true }
    });
    heatmapCtrl.events.add('select', () => this._heatmap.setMap(map));
    heatmapCtrl.events.add('deselect', () => this._heatmap.setMap(null));
    map.controls.add(heatmapCtrl, { float: 'left', floatIndex: 0 });

    const placeCtrl = this._ctrls.placeCtrl = new ns.control.Button({ // eslint-disable-line no-multi-assign
      options: { maxWidth: 200 },
      data: { content: locale.getMessages()['map.button.places'] },
      state: { selected: true }
    });
    placeCtrl.events.add('select', () => map.geoObjects.add(this._objectManager));
    placeCtrl.events.add('deselect', () => map.geoObjects.remove(this._objectManager));
    map.controls.add(placeCtrl, { float: 'left', floatIndex: 0 });
  }

  displayData (data: FeatureCollection) {
    if (!this.map) {
      this._setup();
      this.map.geoObjects.add(this._objectManager);
      this._heatmap.setMap(this.map);
    }
    this.removeAllData();

    this._objectManager.add(data);
    this._setHeatmapData(data);
  }

  _setHeatmapData (data) {
    const totalDistance = data.features.reduce((acc, feature) => acc + feature.properties.totalDistance, 0);
    this._heatmap.setData({
      type: 'FeatureCollection',
      features: data.features.map(feature => Object.assign({}, feature, { properties: { weight: feature.properties.totalDistance / totalDistance } }))
    });
  }

  removeAllData () {
    this._objectManager.removeAll();
  }

  removeMap () {
    this.removeAllData();
    this._objectManager.setParent(null);
    this._heatmap.setMap(null);
    this.map.destroy();
    this.map = null;
  }
}

export default MapController;
