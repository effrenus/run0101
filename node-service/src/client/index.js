// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import FontFaceObserver from 'fontfaceobserver';
import { match, browserHistory, Router } from 'react-router';
import { Provider } from 'react-redux';
import LanguageProvider from './containers/LanguageProvider';
import createStore from './store';
import routes from './routes';

if (process.env.BROWSER) {
  const lobsterObserver = new FontFaceObserver('Lobster');
  lobsterObserver.load().then(
    () => document.body.classList.add('fontLoaded'),
    () => document.body.classList.remove('fontLoaded')
  );

  if (window.Intl) {
    render();
  } else {
    Promise.all([
      import(/* webpackChunkName: "intl" */ 'intl'),
      import(/* webpackChunkName: "intl_en" */ 'intl/locale-data/jsonp/en.js'),
      import(/* webpackChunkName: "intl_ru" */ 'intl/locale-data/jsonp/ru.js'),
      import(/* webpackChunkName: "fetch" */ 'whatwg-fetch')
    ])
      .then(render);
  }
} else {
  render();
}

function render () {
  const store = createStore();

  match({ history: browserHistory, routes }, (error, redirectLocation, renderProps) => {
    ReactDOM.render(
      <Provider store={store}>
      <LanguageProvider>
      <Router {...renderProps} />
      </LanguageProvider>
      </Provider>,
      document.getElementById('root'));
    });
}
