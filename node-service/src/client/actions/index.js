// @flow
import { createAction } from 'redux-act';
import type { ActivitiesDataType } from '../reducers/activitiesData';

export const setActivitiesData = createAction('set activities data');

export const fetchActivitiesData = (year: number): Function => (dispatch: Dispatch<Function>): Promise<*> =>
  fetch(`/api/v1/${year}/data/json/`)
    .then((res: Object) => res.json())
    .then((json: ActivitiesDataType) => dispatch(setActivitiesData(json)));

export const setYear = createAction('set year');

export const changeLocale = createAction('change locale');

export const setActivity = createAction('set activity');

export const fetchActivity = (id: number): Function => (dispatch: Dispatch<Function>): Promise<*> =>
  fetch(`/api/v1/activity/${id}/json/`)
    .then((res: Object) => res.json())
    .then((json: ActivitiesDataType) => dispatch(setActivity(json)));
