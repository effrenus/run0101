// @flow
/* global Headers */
import { urlBase64ToUint8Array } from '../utils';
import { PUBLIC_VAPID_KEY } from '../config';

type SubscriptionType = {
  endpoint: string,
  keys: {
    auth: string,
    p256dh: string
  }
};

const workerPath = '/sw.js';

export const isSWSupported: boolean = typeof navigator !== 'undefined' && 'serviceWorker' in navigator;

export const isPushSupported: boolean = isSWSupported && 'PushManager' in window;

class SWorker {
  _registration: Object;

  constructor () {
    if (isSWSupported && navigator.serviceWorker) {
      if (navigator.serviceWorker.controller) {
        this._registration = navigator.serviceWorker.getRegistration();
      } else {
        this._registration = navigator.serviceWorker.register(workerPath);
      }
    }
  }

  getRegistration () {
    return this._registration;
  }
}

export const sw = new SWorker();

class Notifications {
  _permission: 'denied' | 'default' | 'granted';
  _swPromise: Promise<*>;
  _registration: Object;

  constructor () {
    if (!isPushSupported) {
      return;
    }
    this._permission = Notification.permission;

    if (this._permission !== 'denied') {
      this._swPromise = sw.getRegistration().then((registration) => {
        this._registration = registration;
      });
    }
  }

  static _saveOnServer = (subscription: SubscriptionType) =>
    fetch('/api/v1/webpush', {
      method: 'POST',
      headers: new Headers({ 'Content-type': 'application/json' }),
      body: JSON.stringify(subscription)
    });

  _subscribe () {
    this._registration.pushManager.subscribe({
      userVisibleOnly: true,
      applicationServerKey: urlBase64ToUint8Array(PUBLIC_VAPID_KEY)
    }).then((subscription: SubscriptionType) => {
      Notifications._saveOnServer(subscription);
    });
  }

  isNotificationGranted () {
    return this._permission !== 'denied';
  }

  getSubscription () {
    return this._permission !== 'denied' && process.env.BROWSER ? sw.getRegistration().then(reg => reg.pushManager.getSubscription()) : Promise.resolve(null);
  }

  subscribe () {
    return this._registration ? Promise.resolve()
      .then(() => this._registration.pushManager.getSubscription()
        .then((subscription) => {
          const isSubscribed = subscription !== null;
          if (!isSubscribed) {
            this._subscribe();
          }
          return subscription;
        })) : this._swPromise.then(() => this.subscribe());
  }

  static unsubscribe = () =>
    sw.getRegistration().then(reg => reg.pushManager.getSubscription()).then((subscription) => {
      if (subscription) {
        subscription.unsubscribe();
      }
    });
}

export const notification = new Notifications();
