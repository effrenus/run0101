import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { IntlProvider, FormattedMessage } from 'react-intl';
import locale from '../../../i18n';

class LanguageProvider extends PureComponent {
  render () {
    const { locale: lang } = this.props;
    locale.setLang(lang);

    return (
      <IntlProvider locale={lang} key={lang} messages={locale.getMessages()}>
        {React.Children.only(this.props.children)}
      </IntlProvider>
    );
  }
}

export default connect(
  (state) => ({
    locale: state.locale
  })
)(LanguageProvider);
