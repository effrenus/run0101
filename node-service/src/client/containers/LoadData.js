import React, { Component } from 'react';
import PreloaderIcon from '../components/PreloaderIcon';

type MethodsType = {
  isDataAvailable: Function,
  loadData: Function
};

export default function (methods: MethodsType) {
  return (Klass) => {
    return class extends Component { // eslint-disable-line react/prefer-stateless-function
      constructor () {
        super();
        this.state = { loading: true };
      }

      componentWillReceiveProps (nextProps) {
        if (methods.isDataAvailable(nextProps)) {
          this.setState({ loading: false });
        } else {
          methods.loadData(nextProps);
        }
      }

      componentWillMount () {
        if (methods.isDataAvailable(this.props)) {
          this.setState({ loading: false });
        } else {
          methods.loadData(this.props);
        }
      }

      render () {
        return this.state.loading ? <PreloaderIcon /> : <Klass {...this.props} />;
      }
    };
  };
}
