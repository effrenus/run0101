import React, { Component } from 'react';
import { connect } from 'react-redux';
import type { ActivitiesType, AthletesType, LocationsType } from '../reducers/activitiesData';
import PreloaderIcon from '../components/PreloaderIcon';
import Future from '../components/FutureActivities';
import { fetchActivitiesData } from '../actions';
import { NEXT_YEAR } from '../../config';

class Home extends Component {
  state: { // eslint-disable-line react/sort-comp
    loading: boolean
  };

  props: { // eslint-disable-line react/sort-comp
    params: {
      year: string
    },
    dispatch: Dispatch<A>,
    year: number,
    athletes: AthletesType,
    activities: ActivitiesType,
    locations: LocationsType,
    photos: Array<string>,
    children: Children,
    router: Router
  };

  constructor () { // eslint-disable-line react/sort-comp
    super();
    this.state = { loading: true };
  }

  componentWillMount () {
    const routeYear = parseInt(this.props.params.year, 10);
    const year = this.props.year || NaN;

    if (isNaN(routeYear) || routeYear < 2014 || routeYear > 2020) {
      this.props.router.push('/error/404/');
    }

    if (routeYear === year) {
      this.setState({ loading: false });
    }
  }

  componentDidMount () {
    const routeParamYear = parseInt(this.props.params.year, 10);
    if (routeParamYear !== this.props.year) {
      this.props.dispatch(fetchActivitiesData(routeParamYear));
    }
  }

  componentWillReceiveProps (nextProps) {
    const nextYear = parseInt(nextProps.params.year, 10);
    const year = parseInt(this.props.params.year, 10);

    if (nextYear !== year && nextYear < NEXT_YEAR) {
      this.setState({ loading: true });
      nextProps.dispatch(fetchActivitiesData(nextYear));
    }

    if (nextYear === year && this.state.loading) {
      this.setState({ loading: false });
    }
  }

  render () {
    const year = parseInt(this.props.params.year, 10);

    return year >= NEXT_YEAR // eslint-disable-line no-nested-ternary
      ? <Future year={2018} />
      : (this.state.loading
          ? <PreloaderIcon />
          : React.cloneElement(this.props.children,
            {
              activities: this.props.activities,
              locations: this.props.locations,
              athletes: this.props.athletes,
              photos: this.props.photos,
              year: this.props.year,
            }));
  }
}

export default connect(
  (state) => {
    const { year, activities, locations, athletes, photos } = state.activitiesData;
    return {
      year, activities, locations, athletes, photos
    };
  }
)(Home);
