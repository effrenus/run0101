// @flow
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from './reducers';

type InitialStoreType = {
  activitiesData?: Object,
  locale: string,
  activity?: Object
};

const initialState: InitialStoreType = typeof window !== 'undefined' && window.__INITIAL_STATE__ ? window.__INITIAL_STATE__ : {};

export default () => createStore(
  rootReducer,
  initialState,
  applyMiddleware(thunkMiddleware)
);
