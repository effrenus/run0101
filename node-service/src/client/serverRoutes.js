import React from 'react';
import { Router, IndexRoute, Route, browserHistory } from 'react-router';
import HomeContainer from './containers/Home';
import App from './components/App';
import Home from './components/Home';
import About from './components/About';
import ActivitiesHome from './components/ActivitiesHome';
import ActivityMap from './components/ActivityMap';
import ActivitiesPhoto from './components/ActivitiesPhoto';
import AthletesList from './components/AthletesList';
import ActivityDetail from './components/ActivityDetail';
import NotFound from './components/NotFound';

export default (
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Home} />
      <Route path="/about" component={About} />

      <Route path="/activity/:id" component={ActivityDetail} />
      <Route path="/:year(\\d{4})" component={HomeContainer}>
        <IndexRoute component={ActivitiesHome} />
        <Route path="map" component={ActivityMap} />
        <Route path="athletes" component={AthletesList} />
        <Route path="photo" component={ActivitiesPhoto} />
      </Route>
      <Route path="/error/404" component={NotFound} />
    </Route>
  </Router>
);
