import React from 'react';
import { Router, IndexRoute, Route, browserHistory } from 'react-router';
import App from './components/App';
import Home from './components/Home';
import About from './components/About';
import ActivityMap from './components/ActivityMap';
import AthletesList from './components/AthletesList';
import ActivitiesPhoto from './components/ActivitiesPhoto';
import ActivityDetail from './components/ActivityDetail';
import NotFound from './components/NotFound';
import HomeContainer from './containers/Home';

function redirect (nextState) {
  const year = parseInt(nextState.params.year, 10);
  if (isNaN(year)) {
    browserHistory.push('/error/404/');
  }
}

export default (
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Home} />
      <Route path="/about" component={About} />

      <Route path="/activity/:id/" component={ActivityDetail} />
      <Route path="/:year(\\d{4})" component={HomeContainer} onEnter={redirect}>
        <IndexRoute
          getComponent={(_, cb) =>
            import(/* webpackChunkName: "home" */ './components/ActivitiesHome').then(module => cb(null, module.default))} />
        <Route path="map" component={ActivityMap} />
        <Route path="athletes" component={AthletesList} />
        <Route path="photo" component={ActivitiesPhoto} />
      </Route>
      <Route path="/error/404" component={NotFound} />
    </Route>
  </Router>
);
