// @flow
import { addLocaleData } from 'react-intl';
import ru from 'react-intl/locale-data/ru';
import en from 'react-intl/locale-data/en';
import { DEFAULT_LANG } from '../config';

import ruMessages from './ru';
import enMessages from './en';

type MessagesType = {
  [key: string]: string
};

class Locale {
  _lang: string;
  _messages: MessagesType;

  constructor (lang: string = DEFAULT_LANG) {
    this._lang = lang;

    this._setupMessages();

    addLocaleData([...en, ...ru]);
  }

  _setupMessages () {
    this._messages = this._lang === 'en' ? enMessages : ruMessages;
  }

  setLang (lang: string = DEFAULT_LANG) {
    if (this._lang !== lang) {
      this._lang = lang;
      this._setupMessages();
    }
  }

  getLang (): string {
    return this._lang;
  }

  getMessages (): MessagesType | {} {
    return this._messages ? this._messages : {};
  }

  getMessage (id: string): string {
    return this._messages[id] ? this._messages[id] : '';
  }
}

export default new Locale();
