// @flow
import React from 'react';

declare module 'recharts' {
  declare class BarChart extends React$Component<void, {
    width: number,
    height: number,
    data: Array<{key: number, val: number}>,
    margin: { top?: number, right?: number, bottom?: number, left?: number }
  }, void> {}
  declare class XAxis extends React$Component<void, { dataKey: string }, void> {}
  declare class YAxis extends React$Component<void, { dataKey: string }, void> {}
  declare class Bar extends React$Component<void, { dataKey: string, fill?: string }, void> {}
}
