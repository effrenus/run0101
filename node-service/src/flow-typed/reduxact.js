// @flow

declare module 'redux-act' {
  declare function createReducer (handlers: { [key: Function]: Function }, defaultState: Object): <S, A>(state: S, action: A) => S;
  declare function createAction (description: Function | string): Function;
}
