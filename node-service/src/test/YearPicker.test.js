import React from 'react';
import { shallow, mount, render } from 'enzyme';
import toJson from 'enzyme-to-json';
import YearPicker from '../client/components/YearPicker';

describe("<YearPicker />", function() {
  it("contains spec with an expectation", function() {
    expect(
      mount(<YearPicker />).find('ul').length
    ).toBe(1);
  });

  it("match snapshot", () => {
    const wrapComponent = shallow(<YearPicker />);
    expect(toJson(wrapComponent)).toMatchSnapshot();
  });
});
