#!/bin/sh
set -e

cd /usr/local/app

node ensure-mongo-running.js

if [ -f server/index.compiled.js ]
then
  exec ./node_modules/.bin/pm2-docker \
          -i max \
          --no-daemon \
          start server/index.compiled.js
else
  echo "Can't find server/index.compiled.js file"
  exit 1
fi
