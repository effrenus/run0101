PROJECT_NAME ?= run0101

NODE_DOCKER_PATH := node-service/docker
DEV_NODE_COMPOSE_FILE := $(NODE_DOCKER_PATH)/dev/docker-compose.yml
PROD_NODE_COMPOSE_FILE := $(NODE_DOCKER_PATH)/prod/docker-compose.yml

PHP_DOCKER_PATH := php-service/docker
DEV_PHP_COMPOSE_FILE := $(PHP_DOCKER_PATH)/dev/docker-compose.yml

# DB_PASSWORD ?=

# https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help:
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

init: ## Create docker specific resources.
	${INFO} "Creating shared volumes..."
	@ docker volume create node-static
	@ docker volume create php-static
	${INFO} "Creating network..."
	@ docker network create web

build: ## Build images for local testing/develop.
	${INFO} "Building app... (it can take some time)"
	${INFO} "Node service"
	@ docker-compose -f $(DEV_NODE_COMPOSE_FILE) build --force-rm --no-cache
	${INFO} "PHP service"
	@ docker-compose -f $(DEV_PHP_COMPOSE_FILE) build --force-rm
	${INFO} "nginx"
	@ docker build -t nginx-proxy -f docker/nginx/Dockerfile ./docker/nginx

run: stop ## Run dev images.
	${INFO} "Starting php service..."
	@ docker-compose -f $(DEV_PHP_COMPOSE_FILE) up -d --force-recreate
	${INFO} "Starting node service..."
	@ docker-compose -f $(DEV_NODE_COMPOSE_FILE) up -d --force-recreate
	${INFO} "Starting nginx server..."
	@ docker run -d --rm \
                -v node-static:/shared/node-static \
                -v php-static:/shared/php-static \
                -p 8000:80 \
                --network=web \
                nginx-proxy
	${INFO} "Now open http://localhost:8000"

stop: ## Stop running containers.
	${INFO} "Stopping running containers..."
	@ docker-compose -f $(DEV_NODE_COMPOSE_FILE) down
	@ docker-compose -f $(DEV_PHP_COMPOSE_FILE) down
	@ docker ps -q | xargs -I ARGS docker stop ARGS

clean: ## Remove exited containers and dangling images.
	@ docker images -f dangling=true -q | xargs -I ARGS docker rmi ARGS
	@ docker ps -a -f status=exited -q | xargs -I ARGS docker rm ARGS

test: ## Testing (in progress..).
	@ echo "Test passed"

YELLOW := "\e[1;33m"
NC := "\e[0m"

# Shell Functions
INFO := @bash -c '\
  printf $(YELLOW); \
  echo "=> $$1"; \
  printf $(NC)' SOME_VALUE
