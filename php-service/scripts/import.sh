#!/bin/bash

echo "Starting import to database ${MYSQL_DATABASE}"
mysql -u root --password=${MYSQL_ROOT_PASSWORD} "${MYSQL_DATABASE}" < /dump/20170122.sql
echo "End of import."
