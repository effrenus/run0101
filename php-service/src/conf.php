<?php
/**
 * App const.
 */
define('SERVER_ROOT', $_SERVER['DOCUMENT_ROOT'] . '/');
define('DOCUMENT_ROOT', SERVER_ROOT . 'pages/');
define('WWW_ROOT', '/');
define('PAGE_404', SERVER_ROOT . '404.php');
define('DEBUG', getenv('DEBUG'));
define('DEFAULT_USER_ID', 1);
define('ADMIN_ID', 2);

define('TIME_0101', 1514754000); // 1 jan 00:00 UTC+4 mktime(0,0,0,1,1,2018)
define('TIME_0201', 1514840400); // 2 jan 00:00 UTC+4 mktime(0,0,0,1,2,2018)
define('TIME_1901', 1514926800); // 3 jan 00:00 UTC+4 mktime(0,0,0,1,3,2018)
define('CURRENT_YEAR', 2018);
define('BASE_HREF', '/');

define('KILOMETERS_CACHE_ABRIDGED', SERVER_ROOT . 'templates/static-page/kilometers-' . CURRENT_YEAR . '-abridged.html');
define('KILOMETERS_CACHE_UNABRIDGED', SERVER_ROOT . 'templates/static-page/kilometers-' . CURRENT_YEAR . '.html');

define('TODAY_CACHE', SERVER_ROOT . 'templates/static-page/today-' . CURRENT_YEAR . '.html');
define('GROUP_OPTIONS_CACHE', SERVER_ROOT . 'templates/static-page/groupOptions-' . CURRENT_YEAR . '.html');

// TODO: Choose only one domain and redirect from other.
$verifiedReferrers = array(
    'http://www.run0101.com/' . CURRENT_YEAR,
    'http://www.run0101.com/' . CURRENT_YEAR . '/',
    'http://run0101.com/' . CURRENT_YEAR, 
    'http://run0101.com/' . CURRENT_YEAR . '/'
);

?>
