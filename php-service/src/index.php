<?php

require_once 'conf.php';

function __autoload($className)
{
    $classFilename = strtolower($className) . '.php';

    if (file_exists(SERVER_ROOT . 'core/' . $classFilename)) {
        require_once SERVER_ROOT . 'core/' . $classFilename;
    }

    if (file_exists(SERVER_ROOT . 'classes/' . $classFilename)) {
        require_once SERVER_ROOT . 'classes/' . $classFilename;
    }
}

function require_page($path, $url)
{
    if (App::checkPath($path, $url)) {
        require_once PAGES_ROOT . $url;
    } else {
        require_once PAGE_404;
    }
}

function exception_error_handler($errno, $errstr, $errfile, $errline )
{
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
}

set_error_handler("exception_error_handler");

App::initialize();

if (DEBUG) {
    error_reporting(E_STRICT);
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    $T1 = microtime(true);
}

$url = $_SERVER['REQUEST_URI'];

if ($url !== false) {
    $url = trim($url, '/');
    $path = explode($url, '/');
    
    if (mb_strlen($url) == 0) {
        header('Location: ' . CURRENT_YEAR . '/');
        exit;
    }

    header('Content-Type: text/html; charset=utf-8');

    $templatePage = '404.php';

    if (preg_match('~^' . CURRENT_YEAR . '$~', $url, $matches)) {
        $templatePage = '0101/index.php';
    } elseif (preg_match('~^' . CURRENT_YEAR . '/all~', $url)) {
        $templatePage = '0101/all.php';
    } elseif (preg_match('~^' . CURRENT_YEAR . '/add~', $url)) {
        $templatePage = '0101/rp/add.php';
    } elseif (preg_match('~^delete~', $url)) {
        $templatePage = '0101/rp/delete.php';
    } elseif (preg_match('~^' . CURRENT_YEAR . '/admin~', $url)) {
        $templatePage = 'user/login.php';
    } elseif (preg_match('~^' . CURRENT_YEAR . '/login~', $url)) {
        $templatePage = 'user/rp/login.php';
    } elseif (preg_match('~^' . CURRENT_YEAR . '/logout~', $url)) {
        $templatePage = 'user/rp/logout.php';
    } elseif (preg_match('~^' . CURRENT_YEAR . '/photo$~', $url)) {
        $templatePage = '0101/photo.php';
    } elseif (preg_match('~^partners$~', $url)) {
        $templatePage = 'static-page/index.php';
    }

    require_once DOCUMENT_ROOT . $templatePage;

}

if (DEBUG > 0) {
    $T2 = microtime(true);
    $T = $T2 - $T1;
    echo '<br /><br /><br />' . $T;
}

?>
