String.prototype.trim = function()
{
	return this.replace(/(^\s+)|(\s+$)/g, "");
}

$.fn.checkForm = function() 
{ 
	var name = $("input[name='name']").attr("value");
	var email = $("input[name='email']").attr("value");
	var distance = ($("input[name='distance']").attr("value"));
	var country = $("input[name='country']").attr("value");
	var city = $("input[name='city']").attr("value");
	
	name = name.trim();
	email = email.trim();
	country = country.trim();
	city = city.trim();
	
	distance = parseInt(distance);
	if (distance == '' || isNaN(distance)) 
	{
		distance = 0;  
	}                             
	
	var emailPattern = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	
	var success = true;
	
	if (name == '' || name == ' ')
	{
		$("#notifications .nameError").slideDown();
		success = false;
	}
	else
	{
		$("#notifications .nameError").slideUp();
	}
	
	if (!email.match(emailPattern))
	{
		$("#notifications .emailError").slideDown();
		success = false;
	}
	else
	{
		$("#notifications .emailError").slideUp();
	}
	
	if (distance < 1 || distance > 1000)
	{
		$("#notifications .distanceError").slideDown();
		success = false;
	}
	else
	{
		$("#notifications .distanceError").slideUp();
	}
	
	if (country == '' || country == ' ')
	{
		$("#notifications .countryError").slideDown();
		success = false;
	}
	else
	{
		$("#notifications .countryError").slideUp();
	}
	
	if (city == '' || city == ' ')
	{
		$("#notifications .cityError").slideDown();
		success = false;
	}
	else
	{
		$("#notifications .cityError").slideUp();
	}
	
	return success;
}

$.fn.immediateCheck = function() 
{ 
	$("input[name='name']").keyup(function () {
		$("form").checkForm()  
	});
	
	$("input[name='email']").keyup(function () {
		$("form").checkForm()  
	});
	
	$("input[name='distance']").keyup(function () {
		$("form").checkForm()  
	});
	
	$("input[name='country']").keyup(function () {
		$("form").checkForm()  
	});
	
	$("input[name='city']").keyup(function () {
		$("form").checkForm()  
	});
}

$(document).ready(function(){
	
	$("#infoSitesToggle").click(function() {
		$("#infoSites").slideToggle(100);
	});
	
	$("textarea[name='comment']").keyup(function () {
		var value = $("textarea[name='comment']").val();
		var charsLeft = 60 - value.length;
		$("#charsLeft").text(charsLeft);
		
		if (value.length > 60) 
		{
			$("textarea[name='comment']").val(value.substring(0, 60));
			$("#charsLeft").text("0");
		} 
	});
	
	$("#faqSwitcher").click(function() {
		$(".faq").slideToggle(50);
	});
	
	$(".list .delete").click(function() {
		$id = $(this).find("input").attr("value");
		$kilometer = $(this).parent();
		
		$(this).children("img").attr("src", "/images/ajax-loader.gif");
		
		$.getJSON("/delete/" + $id + "/", function(data) {
			if (data.success == 'true')
			{
				$kilometer.hide();
			}  
		});    
	});
	
	var form = {beingSubmitted: false};
	
	$("form").submit(function(e) {
	  
	  if (form.beingSubmitted)
	  {
  	  return false;
	  }
	  
	  form.beingSubmitted = true;
	  
		if ($("form").checkForm())
		{
			return true;
		}
		else
		{
			$("form").immediateCheck();
			form.beingSubmitted = false;
			return false;
		}
	});
	
  if (typeof(window.localStorage) !== "undefined") 
  {
    window.localStorageEnabled = true;
  }
  else
  {
    window.localStorageEnabled = false;
  }
	
	var language = {
    selected: null,
    rus: $('#rus'),
    eng: $('#eng'),
    select: function(name)
    {
      this.selected = name;
      
      if (window.localStorageEnabled)
      {
        window.localStorage.setItem('userLanguage', name);
      }
      
      switch (name)
      {
        case 'rus':
          this.rus.removeClass('passive').addClass('active');
          this.eng.removeClass('active').addClass('passive');
          $('.topLogo').css('backgroundPosition', '0 0px');
        break;
        case 'eng':
          this.rus.removeClass('active').addClass('passive');
          this.eng.removeClass('passive').addClass('active');
          $('.topLogo').css('backgroundPosition', '0 -74px');
        break;
      }
      
      $('.translatable').each(function() {
        var text = $(this).attr('data-' + name);
        if (this.tagName == 'INPUT')
        {
          $(this).val(text);
        }
        else
        {
          $(this).text(text);
        }
      });
            

    },
    getNavigatorLanguage: function() {
      var userLanguage = window.navigator.userLanguage || window.navigator.language;
      
      if (userLanguage.slice(0,2).toLowerCase() == 'ru')
      {
        return 'rus';
      }
      else
      {
        return 'eng';
      }    
    },
    init: function(name)
    {   
      this.rus.click(function() {
        language.select('rus');
      });
      
      this.eng.click(function() {
        language.select('eng');
      });
      
      language.select(name);
    }

  };

  if (window.localStorageEnabled)
  {
    if (window.localStorage.getItem('userLanguage'))
    {
      language.init(window.localStorage.getItem('userLanguage'));
    }
    else
    {
      language.init(language.getNavigatorLanguage());
    }
    
  }
  else
  {
    language.init(language.getNavigatorLanguage());
  }

    
});
 

