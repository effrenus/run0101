<?php

class Session
{
    public $id = '';
    public $lastVisitTime = '';

    public function __construct()
    {
        $this->id = App::getCookie('session');

        if ($this->id)
        {
            if (!$this->read())
            {
                $this->create();
            }
        }

        else
        {
            $this->create();
        }
    }

    public function read()
    {
        $sql = "SELECT
                            *
                        FROM
                            `sessions`
                        WHERE
                            `id` = '" . $this->id . "';";

        $resultSet = App::getDB()->getObjectResultSet($sql);
        if (count($resultSet) > 0)
        {
            $this->id = $resultSet[0]->id;
            $this->lastVisitTime = $resultSet[0]->lastVisitTime;

            $user = new User();
            $user->id = $resultSet[0]->userId;
            $user->read();
            App::setUser($user);

            return true;
        }
        else
        {
            return false;
        }
    }

    public function update()
    {
        if ($this->id)
        {
            $sql = "UPDATE
                                `sessions`
                            SET
                                `userId` = " . intval(App::getUser()->id) . ",
                                `lastVisitTime` = NOW()
                            WHERE
                                `id` = '" . $this->id . "';";

            App::getDB()->query($sql);
        }
    }

    public function create()
    {
        $user = new User();
        $user->id = DEFAULT_USER_ID;
        App::setUser($user);

        $this->id = md5(uniqid(time()));

        $sql = "INSERT INTO
                            `sessions`
                        SET
                            `id` = '" . $this->id . "',
                            `userId` = " . intval(App::getUser()->id) . ",
                            `lastVisitTime` = NOW();
            ";

        App::getDB()->query($sql);

        App::addCookie('session', $this->id);
    }
}

?>
