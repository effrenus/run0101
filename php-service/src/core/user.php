<?php

	class User
	{
		public $id;
		public $nickname;
		public $passwordMD5;
		public $accessLevel;

		public function create()
		{
			$sql = "INSERT INTO
								`users`
							SET
								`nickname` = '" . mysql_real_escape_string($this->nickname) . "',
								`passwordMD5` = '" . mysql_real_escape_string($this->passwordMD5) . "',
								`accessLevel` = '" . mysql_real_escape_string($this->accessLevel) . "';";

			App::getDB()->query($sql);
			$this->id = App::getDB()->getLastInsertId();
		}

		public function read()
		{
			$sql = "SELECT
								*
							FROM
								`users`
							WHERE
								`id` = " . intval($this->id) . ";";

			$resultSet = App::getDB()->getObjectResultSet($sql);
			if (count($resultSet) > 0)
			{
				$this->nickname = $resultSet[0]->nickname;
				$this->passwordMD5 = $resultSet[0]->passwordMD5;
				$this->accessLevel = $resultSet[0]->accessLevel;
			}
		}
	}

?>