<?php

	class AppObject
	{
		public function setObjectProperties(StdClass $object)
		{
			$properties = get_object_vars($object);
			
			foreach ($properties as $key => $value)
			{
				$this->$key = stripslashes($value);
			}
		}
	}
	
?>