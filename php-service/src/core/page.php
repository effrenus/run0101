<?php

	class Page
	{
		protected $page;  
		public $wwwRoot = WWW_ROOT;
		public $path;
		public $depth;
		
		public function __construct($template)
		{
			$this->page = App::getTemplate($template);
			$this->render();
		}
	}

?>