<?php

    class DB
    {
        private $database;
        private $user;
        private $password;
        private $server;
        public $linkId = false;

        public function __construct()
        {
            $this->database = getenv('DB_DATABASE');
            $this->user = getenv('DB_USER');
            $this->password = getenv('DB_PASSWORD');
            $this->server = getenv('DB_SERVER_NAME');

            $this->linkId = mysqli_connect($this->server, $this->user, $this->password, false);

            if ($this->linkId)
            {
                mysqli_select_db($this->linkId, $this->database);
                mysqli_set_charset($this->linkId, 'utf8');
            }
        }

        public function query($sql)
        {
            if (DEBUG >= 2)
            {
                echo '<br /><br />' . $sql . '<br />';

                $T1 = microtime(true);
            }

            $result = mysqli_query($this->linkId, $sql);

            if (DEBUG >=2 )
            {
                $T2 = microtime(true);
                $T = $T2 - $T1;
                echo $T . '<br /><br />';
            }

            return $result;
        }

        public function getObjectResultSet($sql)
        {
            $result = array();

            $res = $this->query($sql);
            while ($object = mysqli_fetch_object($res))
            {
                $result[] = $object;
            }

            return $result;
        }

        public function getColumn($sql)
        {
            $result = array();

            $res = $this->query($sql);
            while ($array = mysqli_fetch_array($res))
            {
                $result[] = $array[0];
            }

            return $result;
        }

        public function getValue($sql)
        {
            $result = null;

            $res = $this->query($sql);
            while ($array = mysqli_fetch_array($res))
            {
                $result = $array[0];
            }

            return $result;
        }

        public function getLastInsertId()
        {
            return mysqli_insert_id($this->linkId);
        }
    }

?>
