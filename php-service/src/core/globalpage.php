<?php

	class GlobalPage extends Page
	{
		function __construct($template)
		{
			parent::__construct($template);
		}

		public function getKilometersBlock($year = CURRENT_YEAR, $limit = 0)
		{
			$kilometersCache = '';

      switch ($limit)
      {
        case 0:
          $kilometersCacheFile = KILOMETERS_CACHE_UNABRIDGED;
        break;
        case 50:
          $kilometersCacheFile = KILOMETERS_CACHE_ABRIDGED;
        break;
        default:
          $kilometersCacheFile = false;
        break;
      }

      if ($kilometersCacheFile !== false)
      {
        if (file_exists($kilometersCacheFile) && App::getUser()->accessLevel == 0)
        {
          $kilometersCache = file_get_contents($kilometersCacheFile);
        }
      }

      if ($kilometersCache != '' && App::getUser()->accessLevel == 0)
      {
        return $kilometersCache;
      }
      else
      {
        $result = '';
			  $kilometersList = '';

			  $kilometers = Factory::getKilometersByYear($year);

			  $kilometersCount = count($kilometers);

        if ($limit == 0)
        {
          $limit = $kilometersCount;
        }
        else
        {
          $limit = $kilometersCount > $limit ? $limit : $kilometersCount;
        }

			  if ($kilometersCount > 0)
			  {
				  for ($i = 0; $i < $limit; $i++)
				  {
					  $kilometersList .= $this->getKilometersItem($kilometers[$i], $kilometersCount - $i);
				  }

				  eval('$result .= "' . App::getTemplate('kilometersBlock.html') . '";');
			  }
			  else
			  {
				  $result .= '<p>Пока еще никто не бегал. Будь первым!</p>';
			  }

        if ($kilometersCacheFile !== false)
        {
  			  if (App::getUser()->accessLevel == 0)
          {
            $file = fopen($kilometersCacheFile, 'w');
            fwrite($file, $result);
            fclose($file);
          }
        }

        return $result;
      }
		}

		public function getKilometersItem(Kilometer $kilometer, $number)
		{
			$result = '';

      $activity = '';

      switch ($kilometer->activity)
      {
        case 'run':
         $activity = 'бегом';
        break;
        case 'ski':
         $activity = '';
        break;
      }

      $group = $kilometer->getGroup();
      $groupVisibility = $group->id == 0 ? 'groupHidden' : 'groupVisible';

      if (App::getUser()->accessLevel == 0)
			{
				eval('$result .= "' . App::getTemplate('kilometersItem.html') . '";');
			}
			elseif (App::getUser()->accessLevel > 0)
			{
				eval('$result .= "' . App::getTemplate('kilometersItemAdmin.html') . '";');
			}

			return $result;
		}

		public function getTodayPhrase()
		{
		  $todayCache = '';

      if (file_exists(TODAY_CACHE) && App::getUser()->accessLevel == 0)
      {
        $todayCache = file_get_contents(TODAY_CACHE);
      }

      if ($todayCache != '')
      {
        return $todayCache;
      }
      else
      {
        $runnersCount = Factory::getRunnersCountByYear(CURRENT_YEAR);
        $runningKilometersCount = Factory::getRunningKilometersCountByYear(CURRENT_YEAR);

        $skiersCount = Factory::getSkiersCountByYear(CURRENT_YEAR);
        $skiingKilometersCount = Factory::getSkiingKilometersCountByYear(CURRENT_YEAR);

        $runners = '<b>' . $runnersCount . '</b>' . App::declineNumber($runnersCount,
          '<span class="translatable" data-rus=" пробежка" data-eng=" run"> пробежка</span>',
          '<span class="translatable" data-rus=" пробежки" data-eng=" runs"> пробежки</span>',
          '<span class="translatable" data-rus=" пробежек" data-eng=" runs"> пробежек</span>');

        $runningKilometers = '<b>' . $runningKilometersCount . '</b>' . App::declineNumber($runningKilometersCount,
          '<span class="translatable" data-rus=" километр" data-eng=" kilometer"> километр</span>',
          '<span class="translatable" data-rus=" километра" data-eng=" kilometers"> километра</span>',
          '<span class="translatable" data-rus=" километров" data-eng=" kilometers"> километров</span>');

        $skiers = '<b>' . $skiersCount . '</b>' . App::declineNumber($skiersCount,
          '<span class="translatable" data-rus=" другая тренировка" data-eng=" other workout"> другая тренировка</span>',
          '<span class="translatable" data-rus=" другие тренировки" data-eng=" other workouts"> другие тренировки</span>',
          '<span class="translatable" data-rus=" других тренировок" data-eng=" other workouts"> других тренировок</span>');

        $skiingKilometers = '<b>' . $skiingKilometersCount . '</b>' . App::declineNumber($skiingKilometersCount,
          '<span class="translatable" data-rus=" километр" data-eng=" kilometer"> километр</span>',
          '<span class="translatable" data-rus=" километра" data-eng=" kilometers"> километра</span>',
          '<span class="translatable" data-rus=" километров" data-eng=" kilometers"> километров</span>');

        $result = '<p><b><span class="translatable" data-rus="В цифрах" data-eng="Facts and Figures">В цифрах</b><br />';

        if ($runnersCount == 0)
        {
          $result .= '<span class="translatable" data-rus="Пока ни одной пробежки" data-eng="No running yet">Пока ни одной пробежки</span>';
        }
        else
        {
          $result .= $runners . ' — ' . $runningKilometers;
        }

        $result .= '<br />';

        if ($skiersCount == 0)
        {
          $result .= '<span class="translatable" data-rus="И ни одной другой тренировки" data-eng="And no other workouts">И ни одной другой тренировки.</span>';

        }
        else
        {
          $result .= $skiers . ' — ' . $skiingKilometers;
        }

        $result .= '</p>';

        $file = fopen(TODAY_CACHE, 'w');
        fwrite($file, $result);
        fclose($file);

        return $result;
      }
		}

		public function getGroupOptions($year)
		{
		  $groupOptionsCache = '';

      if (file_exists(GROUP_OPTIONS_CACHE) && App::getUser()->accessLevel == 0)
      {
        $groupOptionsCache = file_get_contents(GROUP_OPTIONS_CACHE);
      }

      if ($groupOptionsCache != '')
      {
        return $groupOptionsCache;
      }
      else
      {
        $result = '<option value="0" class="translatable" data-rus="Нет в списке" data-eng="Not in list">Нет в списке</option>';

        $groups = Factory::getGroupsByYear($year);

        foreach ($groups as $group)
        {
          eval('$result .= "' . App::getTemplate('groupOptionItem.html') . '";');
        }

        $file = fopen(GROUP_OPTIONS_CACHE, 'w');
        fwrite($file, $result);
        fclose($file);

        return $result;
      }
		}

		public function getFooter()
		{
			$result = '';

			eval('$result .= "' . App::getTemplate('footer.html') . '";');

			return $result;
		}

		public function getPreviousYears()
		{
			$result = '';

			eval('$result .= "' . App::getTemplate('0101/previousYears.html') . '";');

			return $result;
		}
	}

?>