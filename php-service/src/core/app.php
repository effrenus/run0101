<?php

class App
{
    static private $db = false;
    static private $session = false;
    static private $user = false;

    static public function initialize()
    {
        self::getDB();
        self::getSession();
    }

    static public function login($nickname, $passwordMD5)
    {
        $user = Factory::checkUser($nickname, $passwordMD5);

        if ($user) {
            self::setUser($user);
            self::getSession()->update();
            return true;
        } else {
            return false;
        }
    }

    static public function logout()
    {
        $user = new User();
        $user->id = DEFAULT_USER_ID;
        self::setUser($user);
        self::$session->update();
    }

    static public function getDB()
    {
        if (!self::$db) {
            self::$db = new DB();
        }

        return self::$db;
    }

    static public function getSession()
    {
        if (!self::$session) {
            self::$session = new Session();
        }

        return self::$session;
    }

    static public function getUser()
    {
        if (!self::$user) {
            self::$user = new User();
        }

        return self::$user;
    }

    static public function setUser(User $user)
    {
        self::$user = $user;
    }

    static public function getTemplate($filename)
    {
        $template = trim(file_get_contents(SERVER_ROOT . 'templates/' . $filename));
        $template = addcslashes(trim($template), '"');

        return $template;
    }

    static public function doesStaticTemplateExist($filename)
    {
        return file_exists(SERVER_ROOT . 'templates/static-page/' . $filename);
    }

    static public function isPageExist($pagename)
    {
        return file_exists(SERVER_ROOT . 'pages/' . $pagename);
    }

    static public function getGPC($name, $defaultValue)
    {
        return array_key_exists($name, $_GET) ? $_GET[$name] : (array_key_exists($name, $_POST) ? $_POST[$name] : (array_key_exists($name, $_COOKIE) ? $_COOKIE[$name] : $defaultValue));
    }

    static public function pofigize($string)
    {
        return mb_convert_case(trim($string), MB_CASE_LOWER, "UTF-8");
    }

    static public function textForWeb($string)
    {
        return nl2br(stripslashes($string));
    }

    static public function addCookie($name, $value = '')
    {
        $expire = time() + 31536000;

        $_COOKIE[$name] = $value;

        setcookie($name, $value, $expire, '/');
    }

    static public function getCookie($name)
    {
        if (array_key_exists($name, $_COOKIE)) {
            return $_COOKIE[$name];
        } else {
            return false;
        }
    }

    static public function checkUrl($url)
    {
        if (preg_match("/[a-zA-Z0-9\-]/", $url)) {
            return true;
        } else {
            return false;
        }
    }

    static public function checkMail($email)
    {
        if (preg_match("/^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/", $email)) {
            return true;
        } else {
            return false;
        }
    }

    static public function checkPath(Array $path, $url)
    {
        foreach ($path as $segment) {
            if (!self::checkUrl($segment)) {
                return false;
            }
        }

        return self::isPageExist($url);
    }

    static public function transliterate($string)
    {
        $trans = array('а' => 'a',
                        'б' => 'b',
                        'в' => 'v',
                        'г' => 'g',
                        'д' => 'd',
                        'е' => 'e',
                        'ё' => 'e',
                        'ж' => 'zh',
                        'з' => 'z',
                        'и' => 'i',
                        'й' => 'j',
                        'к' => 'k',
                        'л' => 'l',
                        'м' => 'm',
                        'н' => 'n',
                        'о' => 'o',
                        'п' => 'p',
                        'р' => 'r',
                        'с' => 's',
                        'т' => 't',
                        'у' => 'u',
                        'ф' => 'f',
                        'х' => 'h',
                        'ц' => 'c',
                        'ч' => 'ch',
                        'ш' => 'sh',
                        'щ' => 'sch',
                        'ы' => 'y',
                        'э' => 'e',
                        'ю' => 'yu',
                        'я' => 'ya',
                        'ь' => '',
                        'ъ' => '',
                        ' ' => '-');

        $string = self::pofigize($string);
        $result = strtr($string, $trans);

        return $result;
        }

    static public function br2nl($string)
    {
        $string = str_replace('<br />', '', $string);
        $string = str_replace('<br>', '', $string);

        return $string;
    }

    static public function dateToMonthAndYear($mysqlDate)
    {
        $yearNumber = intval(substr($mysqlDate, 0, 4));
        $monthNumber = intval(substr($mysqlDate, 5, 2)) - 1;

        $monthNamesLowercase = array('январь',
                                        'февраль',
                                        'март',
                                        'апрель',
                                        'май',
                                        'июнь',
                                        'июль',
                                        'август',
                                        'сентябрь',
                                        'октябрь',
                                        'ноябрь',
                                        'декабрь');

        $string = $monthNamesLowercase[$monthNumber] . ' ' . $yearNumber;

        return $string;
    }

    static public function dateToHumanDate($mysqlDate)
    {
        $yearNumber = intval(substr($mysqlDate, 0, 4));
        $monthNumber = intval(substr($mysqlDate, 5, 2)) - 1;
        $dayNumber = intval(substr($mysqlDate, 8, 2));

        $monthNamesLowercase = array('января',
                                        'февраля',
                                        'марта',
                                        'апреля',
                                        'мая',
                                        'июня',
                                        'июля',
                                        'августа',
                                        'сентября',
                                        'октября',
                                        'ноября',
                                        'декабря');

        $string = $dayNumber . ' ' . $monthNamesLowercase[$monthNumber] . ' ' . $yearNumber;

        return $string;
    }

    static public function declineNumber($number, $one, $twoToFour, $fiveToTen)
    {
        $nmod10 = $number % 10;
        $nmod100 = $number % 100;

        if (($number == 1) || ($nmod10 == 1 && $nmod100 != 11)) {
            $result = $one;
        } elseif (($nmod10 > 1) && ($nmod10 < 5) && ($nmod100 != 12 && $nmod100 != 13 && $nmod100 != 14)) {
            $result = $twoToFour;
        } else {
            $result = $fiveToTen;
        }

        return $result;
    }

    static public function ucfirst($string)
    {
        $ucstring = mb_strtoupper(mb_substr($string, 0, 1, 'UTF-8'), 'UTF-8') . mb_substr($string, 1, mb_strlen($string) - 1, 'UTF-8');

        return $ucstring;
    }

    static public function getIP()
    {
        return $_SERVER['REMOTE_ADDR'];
    }

    static public function raiseError()
    {
        echo 'Возникла неизвестная ошибка. Напишите об обстоятельствах по адресу andrushkaaaa@gmail.com';
    }

    static public function checkUserText($string)
    {
        return trim(htmlspecialchars($string));
    }
}
?>
