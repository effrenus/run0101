<?php

	class Shoe
	{
		public $id = 0;
		public $name = '';

		public function check()
		{
			return true;
		}

		public function create()
		{
			if ($this->check())
			{
				$sql = "INSERT INTO
									`shoes`
								SET
									`name` = '" . mysql_real_escape_string($this->name) . "';";

				$result = App::getDB()->query($sql);
				$this->id = App::getDB()->getLastInsertId();

				return $result;
			}
		}

		public function read()
		{
			$sql = "SELECT * FROM
								`shoes`
							WHERE
								`id` = " . intval($this->id) . ";";

			$resultSet = App::getDB()->getObjectResultSet($sql);
			if (count($resultSet) > 0)
			{
				$item = $resultSet[0];
				$this->name = $item->name;
			}
		}

		public function update()
		{
			if ($this->check())
			{

			}
		}

		public function delete()
		{
			$sql = "DELETE FROM
								`shoes`
							WHERE
								`id` = " . intval($this->id) . ";";

			App::getDB()->query($sql);
		}
	}

?>