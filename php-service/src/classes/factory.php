<?php

	class Factory
	{
		static public function checkUser($nickname, $passwordMD5)
		{
			$sql = "SELECT
								`id`
							FROM
								`users`
							WHERE
								`nickname` = '" . mysql_real_escape_string($nickname) . "'
								AND `passwordMD5` = '" . mysql_real_escape_string($passwordMD5) . "';";

			$resultSet = App::getDB()->getObjectResultSet($sql);

			if (count($resultSet) > 0)
			{
				$user = new User();
				$user->id = $resultSet[0]->id;
				$user->read();

				return $user;
			}
			else
			{
				return false;
			}
		}

		static public function getShoesById(array $ids, $orderBy = 'id')
		{
			$result = array();

			if (count($ids) > 0)
			{
				$sql = "SELECT
									*
								FROM
									`shoes`
								WHERE
									`id` IN (" . implode(', ', $ids) . ")
								ORDER BY
									`" . $orderBy . "` ASC";

				$resultSet = App::getDB()->getObjectResultSet($sql);
				$result = array();

				foreach ($resultSet as $item)
				{
					$shoe = new Shoe();
					$shoe->id = $item->id;
					$shoe->name = $item->name;
					$result[] = $shoe;
				}
			}

			return $result;
		}

		static public function getAllShoes()
		{
			$sql = "SELECT
								`id`
							FROM
								`shoes`
							WHERE
								`id` != '" . DEFAULT_SHOES_ID . "'
							ORDER BY
								`name` ASC;";

			$ids = App::getDB()->getColumn($sql);

			return self::getShoesById($ids, 'name');
		}

		static public function getKilometersById(array $ids)
		{
			$result = array();

			if (count($ids) > 0)
			{
				$sql = "SELECT
									*
								FROM
									`kilometers`
								WHERE
									`id` IN (" . implode(', ', $ids) . ")
								ORDER BY
									`id` DESC;";

				$resultSet = App::getDB()->getObjectResultSet($sql);
				$result = array();

				foreach ($resultSet as $item)
				{
					$kilometer = new Kilometer();
					$kilometer->setObjectProperties($item);
					$result[] = $kilometer;
				}
			}

			return $result;
		}

		static public function getGroupsById(array $ids)
		{
			$result = array();

			if (count($ids) > 0)
			{
				$sql = "SELECT
									*
								FROM
									`groups`
								WHERE
									`id` IN (" . implode(', ', $ids) . ")
								ORDER BY
									`city` ASC, `name` ASC;";

				$resultSet = App::getDB()->getObjectResultSet($sql);
				$result = array();

				foreach ($resultSet as $item)
				{
					$group = new Group();
					$group->setObjectProperties($item);
					$result[] = $group;
				}
			}

			return $result;
		}


		static public function getGroupsByYear($year)
		{
			$sql = "SELECT
								`id`
							FROM
								`groups`
							WHERE
								`year` = " . intval($year) . "
							ORDER BY
								`id` DESC;";

			$ids = App::getDB()->getColumn($sql);

			return self::getGroupsById($ids);
		}

		static public function getKilometersByYear($year)
		{
			$sql = "SELECT
								`id`
							FROM
								`kilometers`
							WHERE
								`year` = " . intval($year) . "
							AND
								`isValid` = 1
							ORDER BY
								`id` DESC;";

			$ids = App::getDB()->getColumn($sql);

			return self::getKilometersById($ids);
		}



		static public function getRunningKilometersCountByYear($year)
		{
			$sql = "SELECT
								SUM(`km`)
							FROM
								`kilometers`
							WHERE
								`year` = " . intval($year) . "
              AND
                `activity` = 'run'
							AND
								`isValid` = 1;";

			$result = App::getDB()->getValue($sql);

			return $result;
		}

		static public function getRunnersCountByYear($year)
		{
			$sql = "SELECT
								COUNT(*)
							FROM
								`kilometers`
							WHERE
								`year` = " . intval($year) . "
							AND
                `activity` = 'run'
              AND
								`isValid` = 1;";

			$result = App::getDB()->getValue($sql);

			return $result;
		}

    static public function getSkiingKilometersCountByYear($year)
    {
      $sql = "SELECT
                SUM(`km`)
              FROM
                `kilometers`
              WHERE
                `year` = " . intval($year) . "
              AND
                `activity` = 'ski'
              AND
                `isValid` = 1;";

      $result = App::getDB()->getValue($sql);

      return $result;
    }

    static public function getSkiersCountByYear($year)
    {
      $sql = "SELECT
                COUNT(*)
              FROM
                `kilometers`
              WHERE
                `year` = " . intval($year) . "
              AND
                `activity` = 'ski'
              AND
                `isValid` = 1;";

      $result = App::getDB()->getValue($sql);

      return $result;
    }

		static public function getMenCountByYear($year)
		{
			$sql = "SELECT
								SUM(`km`)
							FROM
								`kilometers`
							WHERE
								`year` = " . intval($year) . "
							AND
								`isValid` = 1
							AND
								`sex` = 1;";

			$result = App::getDB()->getValue($sql);
			empty($result) ? $result = 0 : $result = $result;

			return $result;
		}

		static public function getWomenCountByYear($year)
		{
			$sql = "SELECT
								SUM(`km`)
							FROM
								`kilometers`
							WHERE
								`year` = " . intval($year) . "
							AND
								`isValid` = 1
							AND
								`sex` = 0;";

			$result = App::getDB()->getValue($sql);
			empty($result) ? $result = 0 : $result = $result;

			return $result;
		}
	}

?>