<?php
/**
 * Write proper description.
 */
class Kilometer extends AppObject
{
    public $id = 0;
    public $name = '';
    public $surname = '';
    public $email = '';
    public $sex = '';
    public $km = 0;
    public $activity = '';
    public $country = '';
    public $city = '';
    public $groupId = 0;
    public $comment = '';
    public $year = '';
    public $creationDate = '';
    public $ip = '';
    public $sessionHash = '';
    public $isValid = '';

    public static function getById($id)
    {
        return new Kilometer($id);
    }

    /**
     * Constructor.
     *
     * @param integer $id
     */
    public function __construct($id = 0)
    {
        if ($id != 0) {
            $this->id = $id;
            $this->read();
        }
    }

    public function create()
    {
        $db_link = App::getDB()->linkId;
        $sql = "INSERT INTO
                            `kilometers`
                        SET
                            `name` = '" . mysqli_real_escape_string($db_link, $this->name) . "',
                            `surname` = '" . mysqli_real_escape_string($db_link, $this->surname) . "',
                            `email` = '" . mysqli_real_escape_string($db_link, $this->email) . "',
                            `sex` = " . intval($this->sex) . ",
                            `km` = " . intval($this->km) . ",
                            `activity` = '" . mysqli_real_escape_string($db_link, $this->activity) . "',
                            `country` = '" . mysqli_real_escape_string($db_link, $this->country) . "',
                            `city` = '" . mysqli_real_escape_string($db_link, $this->city) . "',
                            `groupId` = " . intval($this->groupId) . ",
                            `comment` = '" . mysqli_real_escape_string($db_link, $this->comment) . "',
                            `year` = " . intval($this->year) . ",
                            `creationDate` = NOW(),
                            `ip` = '" . mysqli_real_escape_string($db_link, $this->ip) . "',
                            `sessionHash` = '" . mysqli_real_escape_string($db_link, $this->sessionHash) . "',
                            `isValid` = " . intval($this->isValid) . ";";

        $result = App::getDB()->query($sql);
        $this->id = App::getDB()->getLastInsertId();

        try {
            file_put_contents(KILOMETERS_CACHE_ABRIDGED, '');
            file_put_contents(KILOMETERS_CACHE_UNABRIDGED, '');
            file_put_contents(TODAY_CACHE, '');
        }
        catch (Exception $e) {
            echo $e;
        }

        return $result;
    }

    public function read()
    {
        $sql = "SELECT
                            *
                        FROM
                            `kilometers`
                        WHERE
                            `id` = " . intval($this->id) . "
                        AND
                            `isValid` = 1;";

        $resultSet = App::getDB()->getObjectResultSet($sql);
        if (count($resultSet) > 0) {
            $item = $resultSet[0];
            $this->setObjectProperties($item);
        }
    }

    public function delete()
    {
        $sql = "UPDATE
                            `kilometers`
                        SET
                            `isValid` = 0
                        WHERE
                            `id` = " . intval($this->id) . ";";

        App::getDB()->query($sql);

        try {
            file_put_contents(KILOMETERS_CACHE_ABRIDGED, '');
            file_put_contents(KILOMETERS_CACHE_UNABRIDGED, '');
            file_put_contents(TODAY_CACHE, '');
        } catch (Exception $e) {

        }
    }

    public function getGroup()
    {
        $result = new Group($this->groupId);

        return $result;
    }
}

?>
