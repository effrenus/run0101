<?php

	class StaticPage extends GlobalPage
	{
		public $path;
		
		function __construct($template, $path)
		{                             
			$this->path = $path;
			
			parent::__construct('static-page/' . $template);
		}
		
		public function render()
		{
			$footer = $this->getFooter();
      $previousYears = $this->getPreviousYears();
      
      eval('$this->page = "' . $this->page . '";');
			echo $this->page;  
		}
	}
	
	if (App::checkUrl($path[0]))
	{
		if (App::doesStaticTemplateExist($path[0] . '.html'))
		{
			$page = new StaticPage($path[0] . '.html', $path);  
		}
		else
		{
			require_once PAGE_404;
		}
	}
	else
	{
		require_once PAGE_404;
	}
		
	
?>