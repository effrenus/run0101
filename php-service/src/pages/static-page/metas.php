<?php

	$metas = array('index' => 
									 array('description' => 'Редакция сайта OptimalTravel.Ru публикует отчеты о поездках и путешествиях',
												 'keywords' => 'Отчет поездка путешествие',
												 'title' => 'Отчеты о поездках'),
								 
								 'bronirovanie-hotel' => 
									 array('description' => 'Бронирование отеля',
												 'keywords' => 'Отель бронирование hotel reserving',
												 'title' => 'Бронирование отелей'),
									 
								 'bronirovanie-aviabilet' => 
									 array('description' => '',
												 'keywords' => '',
												 'title' => ''),
												 
								 'bronirovanie-jd-bilet' => 
									 array('description' => '',
												 'keywords' => '',
												 'title' => ''),
												 
								 'arenda-mashina' => 
									 array('description' => '',
												 'keywords' => '',
												 'title' => ''),
												 
								 'poisk-poputchika' => 
									 array('description' => 'Ссылки на сайты и сервисы по поиску попутчика для поездок и путешествий',
												 'keywords' => 'поиск попутчик поездка путешествие',
												 'title' => 'Сайты по поиску попутчика для путешествий и поездок'),
												 
								 'obuchenie-zarubezhom' => 
									 array('description' => '',
												 'keywords' => '',
												 'title' => ''),
												 
								 'foto-video-turizm' => 
									 array('description' => 'Подборка видеороликов и фотографий о путешествиях, туризме, полетах самолетов',
												 'keywords' => 'Видеоролик фотография путешествие',
												 'title' => 'Видеоролики и фотографии о путешествиях и поездках'),
												 
								 'feedback' => 
									 array('description' => 'Форма отбратной связи с разработчиками и редакторами проекта optimaltravel.ru',
												 'keywords' => 'путешествия optimaltravel optimal travel',
												 'title' => 'Неравнодушны к путешествиям? Свяжитесь с нами!'),
												 
								 'livejournal' => 
									 array('description' => 'ЖЖ OptimalTravel в LiveJournal',
												 'keywords' => 'OptimalTravel LiveJournal жж',
												 'title' => 'OptimalTravel в LiveJournal'),
												 
								 'vkontakte-club' => 
									 array('description' => '',
												 'keywords' => '',
												 'title' => ''),
												 
								 'novosti-turizm' => 
									 array('description' => 'Новости о туризме, авиакомпаниях, цифровой технике для туристов',
												 'keywords' => 'Новости туризм путешествие',
												 'title' => 'Новости туризма и путешествий'),
												 
								 'turizm-viza' => 
									 array('description' => 'Информация о туристических визах, безвизовых странах, фото шенгенской визы (шенгена)',
												 'keywords' => 'туристические виза фото шенгенская безвизовая шенген страна',
												 'title' => 'Туристические визы, безвизовые страны, фото шенгенской визы (шенгена)'),
												 
								 'putevoditel' => 
									 array('description' => 'Путеводители по странам: Испания, Италия, Крым и другие страны',
												 'keywords' => 'путеводитель страна мир путешествие',
												 'title' => 'Путеводители по странам. Испания, Италия, США...'),
												 
								 'sait-turizm' => 
									 array('description' => 'Полезные сайты о туризме и путешествиях',
												 'keywords' => 'сайт туризм путешествия',
												 'title' => 'Сайты о туризме'),
												 
								 'about' => 
									 array('description' => 'О проекте OptimalTravel.Ru',
												 'keywords' => 'проект OptimalTravel.Ru',
												 'title' => 'О проекте OptimalTravel.Ru'),
												 
								 'english' => 
									 array('description' => 'About OptimalTravel.Ru project. Contacts, phone, e-mail',
                                                 'keywords' => 'optimaltravel optimal travel',
                                                 'title' => 'About OptimalTravel.Ru project'),
                                    			 
								 'advertising' => 
									 array('description' => 'Информация о размещении рекламы на сайте о туризме и путешествиях OptimalTravel.Ru',
												 'keywords' => 'размещение рекламы сайт туризм путешествия OptimalTravel.Ru',
												 'title' => 'Размещение рекламы на сайте о туризме и путешествиях OptimalTravel.Ru'),
												 
								 'partnership' => 
									 array('description' => 'Информация о партнерстве с сайтом о туризме и путешествиях OptimalTravel.Ru',
                                                 'keywords' => 'партнерство реклама сайт туризм путешествия OptimalTravel.Ru',
                                                 'title' => 'Партнерство с сайтом о туризме и путешествиях OptimalTravel.Ru'),
                                    			 
								 'sitemap' => 
									 array('description' => 'Карта сайта о туризме и путешествиях OptimalTravel.Ru',
												 'keywords' => 'карта сайта туризм путешествия OptimalTravel.Ru',
												 'title' => 'Карта сайта OptimalTravel.Ru')   
								 );
	

?>   