<?php
/**
 * Processing form data.
 */
$currentTime = time();

if ($currentTime >= TIME_1901 && !DEBUG) {
    echo 'Время вышло. Результаты вмерзли в историю';
    exit;
}

// Проверяем, откуда пришла форма.
$isRefererVerified = (array_key_exists('HTTP_REFERER', $_SERVER) && 
                     in_array($_SERVER['HTTP_REFERER'], $verifiedReferrers)) || 
                     DEBUG;

if (!$isRefererVerified) {
    echo '';
    exit;
}

function getNumberField($name, $defaultValue = '')
{
    return intval(App::getGPC($name, $defaultValue));
}

function getStringField($name, $maxChars = -1, $defaultValue = '')
{
    $field = stripslashes(
        App::getGPC($name, $defaultValue)
    );

    if ($maxChars != -1) {
        $field = mb_substr($field, 0, $maxChars, 'utf-8');
    }

    return trim($field);
}

function getEscapedStringField($name, $maxChars = -1, $defaultValue = '')
{
    return htmlspecialchars(getStringField($name, $maxChars, $defaultValue));
}

// Принимаем все переменные
$groupId = getNumberField('groupId');
$name = getEscapedStringField('name', 30);
$email = getStringField('email', 60);
$sex = getNumberField('sex');
$distance = getNumberField('distance');
$activity = getEscapedStringField('activity', 10);
$shoes = getNumberField('shoes');
$country = getEscapedStringField('country', 30);
$city = getEscapedStringField('city', 30);
$comment = getEscapedStringField('comment', 60);

$year = CURRENT_YEAR;
$ip = App::getIP();
$sessionHash = App::getSession()->id;
$isValid = 1;

if (empty($name) OR $name == ' ') {
    echo 'Введите имя полностью';
    exit;
}

if (!App::checkMail($email)) {
    echo 'Введите e-mail корректно';
    exit;
}

if ($sex != 0 AND $sex != 1) {
    echo 'Введите пол корректно';
    exit;
}

if ($distance < 1 OR $distance > 1000) {
    echo 'Мы думаем, что ваша дистанция сегодня была больше 0, но меньше 1000 км!';
    exit;
}

if (empty($country)) {
    echo 'Введите страну';
    exit;
}

if (empty($city)) {
    echo 'Введите город';
    exit;
}

// Записываем
$kilometer = new Kilometer();
$kilometer->groupId = $groupId;
$kilometer->name = $name;
$kilometer->email = $email;
$kilometer->sex = $sex;
$kilometer->km = $distance;
$kilometer->activity = $activity;
$kilometer->shoes = 0;
$kilometer->country = $country;
$kilometer->city = $city;
$kilometer->comment = $comment;
$kilometer->year = $year;
$kilometer->ip = $ip;
$kilometer->sessionHash = $sessionHash;
$kilometer->isValid = $isValid;

if ($kilometer->create()) {
    header('Location: ' . BASE_HREF . CURRENT_YEAR . '/#you');
    exit;
} else {
    App::raiseError();
    exit;
}

?>
