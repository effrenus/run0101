<?php

$time = time();

if ($time >= TIME_1901) {
    // echo 'Время вышло. Результаты вмерзли в историю'; exit;
}

// Принимаем все переменные

$id = $path[1];

// Проверяем пользователя

if (App::getUser()->accessLevel > 0) {
    $kilometer = Kilometer.getById($id);
    $kilometer->delete();
    
    $result = array('success' => 'true');
    echo json_encode($result);
} else {
    $result = array('success' => 'false');
    echo json_encode($result);
}

?>
