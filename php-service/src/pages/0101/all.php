<?php

	class Page0101 extends GlobalPage
	{
		public $depth;
		public $path;

		function __construct($template, $path = array())
		{
			$this->depth = count($path);
			$this->path = $path;

			parent::__construct($template);
		}

		public function getShoesBlock()
		{
			$result = '';

			$shoes = Factory::getAllShoes();
			$shoesList = '';

			$shoesList .= '<option value="' . DEFAULT_SHOES_ID . '"> Нет в списке </option>';

			foreach ($shoes as $shoe)
			{
				$shoesList .= $this->getShoesItem($shoe);
			}

			eval('$result .= "' . App::getTemplate('0101/shoesBlock.html') . '";');

			return $result;
		}

		public function getShoesItem(Shoe $shoe)
		{
			$result = '';

			eval('$result .= "' . App::getTemplate('0101/shoesItem.html') . '";');

			return $result;
		}

		public function getFaq()
		{
			$result = '';

			eval('$result .= "' . App::getTemplate('0101/faq.html') . '";');

			return $result;
		}

		public function getInfoSponsors()
		{
			$result = '';

			eval('$result .= "' . App::getTemplate('0101/infoSponsors.html') . '";');

			return $result;
		}

    public function getSummary()
    {
      $result = '';

      eval('$result .= "' . App::getTemplate('0101/summary.html') . '";');

      return $result;
    }

    public function getSmallBanner()
		{
			$result = '';

			eval('$result .= "' . App::getTemplate('0101/smallBanner.html') . '";');

			return $result;
		}

		public function getTopBanner()
		{
			$result = '';

			eval('$result .= "' . App::getTemplate('0101/topBanner.html') . '";');

			return $result;
		}

		public function getMenVsWomen()
		{
			$result = '';

			$runnersCount = Factory::getRunnersCountByYear(CURRENT_YEAR);

			if ($runnersCount > 0)
			{
				$menCount = Factory::getMenCountByYear(CURRENT_YEAR);
				$womenCount = Factory::getWomenCountByYear(CURRENT_YEAR);

				eval('$result .= "' . App::getTemplate('0101/menVsWomen.html') . '";');
			}

			return $result;
		}

		public function render()
		{
			if (App::getUser()->accessLevel > 0)
			{
				$isAdmin = 'admin';
			}
			else
			{
				$isAdmin = '';
			}

      eval('$previousYears = "' . App::getTemplate('0101/previousYears.html') . '";');
			// $menVsWomen = $this->getMenVsWomen();
			$topBanner = $this->getTopBanner();
			// $smallBanner = $this->getSmallBanner();
      $infoSponsors = $this->getInfoSponsors();
      $summary = $this->getSummary();
			$todayPhrase = $this->getTodayPhrase();
			$faq = $this->getFaq();
			$kilometersBlock = $this->getKilometersBlock(CURRENT_YEAR, 0);
      // $shoesBlock = $this->getShoesBlock();
			$footer = $this->getFooter();

      $currentYear = CURRENT_YEAR;

			eval('$this->page = "' . $this->page . '";');
			echo $this->page;
		}
	}

	$page = new Page0101('0101/all.html', $path);

?>